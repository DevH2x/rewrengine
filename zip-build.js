const zip = require('zip-folder');

zip('dist', 'build.zip', (err) => {
  if (err) {
    throw new Error('[ZIP BUILD ERROR]: ' + err);
  } else {
    Debug.log('Build Zipped');
  }
});