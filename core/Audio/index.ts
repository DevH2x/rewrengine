import { EventEmitter } from "../Event";
import { Howl, HowlOptions } from "howler";
import { ResourceManager } from "../Resources";
import { Debug } from '../Common';

export class AudioDispatcher extends EventEmitter {
  public soundList: { [key: string]: Howl } = {}

  public import (src: string, options: HowlOptions): Howl {
    const name = src.split('/').pop().split('.')[0];
    this.soundList[name] = new Howl({
      src: [ src ],
      ...options
    });

    ResourceManager.registerResource(this);

    this.soundList[name].once('load', () => {
      this.triggerEvent('load', this.soundList[name]);
    });

    return this.soundList[name];
  }

  public play(name: string, loop = false): void {
    const s = this.soundList[name];
    if (!s) {
      Debug.error('Sound', `"${name}"`, 'was not imported');
      return;
    }
    s.loop(loop);
    s.play();
  }

  public stop (name: string): void {
    const s = this.soundList[name];
    s.stop();
  }

  public fadePlay(name: string, duration: number): void {
    const s = this.soundList[name];
    if (s.playing()){
      s.play();
      s.fade(0, 1, duration);
    }
  }
  public fadeStop(name: string, duration: number): void {
    const s = this.soundList[name];
    if (s.playing()){
      s.fade(1, 0, duration);
      setTimeout(() => { s.stop(); }, duration);
    }
  }
}