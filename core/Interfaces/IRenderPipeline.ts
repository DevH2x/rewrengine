export interface IRenderPipeline {
  beforeUpdate?(c?: CanvasRenderingContext2D): void,
  update?(c?: CanvasRenderingContext2D): void,
  afterUpdate?(c?: CanvasRenderingContext2D): void
}