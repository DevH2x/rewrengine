import { CacheManager, ResourceManager } from "../Resources";
import { EventEmitter } from "../Event";

export interface ITilesetOptions {
  url: string
  tileWidth: number,
  tileHeight: number,
  columns?: number,
  preventLoad?: boolean
}

export class Tileset extends EventEmitter {
  private img: HTMLImageElement = new Image();
  public cached = false;

  constructor (private options: ITilesetOptions) {
    super();
    if (!options.preventLoad) {
      this.applyOptions();
    }
  }

  private applyOptions (): void {
    if (!this.getCache()) {
      this.initEventListeners();
      this.applyURL();
    } else {
      setTimeout(() => this.triggerEvent('load:cached'), 100);
    }
  }

  private calculateColumns(): void {
    this.options.columns = (this.img.width / this.options.tileWidth) >> 0;
  }

  private getCache() {
    const cachedImg = CacheManager.get(this.options.url);
    if (cachedImg) {
      this.img = cachedImg;
      this.calculateColumns();
      this.cached = true;
      this.triggerEvent('load', this);
    }

    return !!this.cached;
  }

  private applyURL (): void {
    setTimeout(() => this.img.src = this.options.url);
  }

  private initEventListeners (): void {
    ResourceManager.registerResource(this);
    this.img.addEventListener('load', () => {
      CacheManager.add(this.options.url, this.img);
      this.calculateColumns();
      setTimeout(() => {
        this.triggerEvent('load', this);
      }, 1000);
    }, { once: true });

    this.img.addEventListener('error', () => {
      this.triggerEvent('error', this);
    }, { once: true });
  }

  public drawTile(index: number, x: number, y: number, c: CanvasRenderingContext2D): void {
    const { options, img } = this;
    const tx = index % options.columns || 0;
    const ty = index / options.columns >> 0;

    c.drawImage(
      img, 
      tx * options.tileWidth, 
      ty * options.tileHeight, 
      options.tileWidth, 
      options.tileHeight, 
      x * options.tileWidth, 
      y * options.tileHeight, 
      options.tileWidth, 
      options.tileHeight
    );
  }

  public load (): void {
    this.applyOptions();
  }
}