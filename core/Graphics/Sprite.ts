/**
 * @file Sprite.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { CacheManager, ResourceManager } from "@core";
import { Debug, Vector2 } from "../Common";
import { EventEmitter } from "../Event";

type GlobalCompositeOperations = 
  'source-over' | 
  'source-out' | 
  'source-in' | 
  'source-atop' | 
  'destination-over' | 
  'destination-in' | 
  'destination-out' | 
  'destination-atop' | 
  'lighter' |
  'copy' | 
  'xor' | 
  'multiply' |
  'screen' | 
  'overlay' |
  'darken' |
  'lighten' |
  'color-dodge' |
  'color-burn' |
  'hard-light' |
  'soft-light' |
  'difference' | 
  'exclusion' |
  'hue' |
  'saturation' |
  'color' |
  'luminosity'

interface SpriteOptions {
  url: string;
  animationSpeed?: number;
  frameSize?: Vector2;
  offset?: Vector2;
  scale?: Vector2;
  blendMode?: GlobalCompositeOperations,
  opacity?: number;
}

export enum SpriteStatus {
  NO_IMAGE = 0,
  LOADED = 1,
  ERROR = 2
} 


export class Sprite extends EventEmitter {
  private url: string;
  private image: HTMLImageElement;
  private frameSize: Vector2;
  private status: SpriteStatus = SpriteStatus.NO_IMAGE;
  private blendMode: GlobalCompositeOperations;
  private cached = false;

  public framesCount: Vector2 = new Vector2(0, 0);
  
  public animationSpeed = 1;
  /**
   * Offset of the sprite in percentage
   */
  public offset: Vector2 = new Vector2(0, 0);

  /**
   * Scale of the sprite in percentage
   */
  public scale: Vector2 = new Vector2(1, 1);

  /**
   * Current frame of the sprite
   */
  public currentFrame = 0;

  /**
   * Angle of the sprite
   */
  public angle = 0;

  /**
   * Opacity of the sprite
   */
  public opacity = 1;

  constructor (
    private options: SpriteOptions
  ) {
    super();
    this.applyOptions(options);
    this.createEventListeners();
  }

  private applyOptions(options: SpriteOptions) {
    for (const key in options) {
      this[key] = options[key];
    }

    this.image = CacheManager.get(this.url);
    if (!this.image) {
      this.image = new Image();
      this.image.src = this.url;
    } else {
      this.cached = true; 
      this.status = SpriteStatus.LOADED;
      this.presetFrameSize();
    }
  }

  private createEventListeners() {
    if (this.cached) {
      Debug.log('[LOADED CACHED]', this.image.src);
      return;
    }

    const onImageLoaded = ({ target }) => {
      this.status = SpriteStatus.LOADED;
      this.presetFrameSize();
      this.triggerEvent('load', this);

      CacheManager.add(this.url, target);
    };
    
    const onImageError = () => {
      this.status = SpriteStatus.ERROR;
      this.triggerEvent('error', this);
    };

    ResourceManager.registerResource(this);

    this.image.addEventListener('load', onImageLoaded, { once: true });
    this.image.addEventListener('error', onImageError, { once: true });
  }

  private presetFrameSize () {
    if (!this.frameSize) {
      this.frameSize = new Vector2(this.image.width, this.image.height);
    } else {
      this.calculateFrames();
    }
  }

  private calculateFrames () {
    const { framesCount, frameSize, image } = this;
    const { x: width, y: height } = frameSize;

    framesCount.x = image.width / width >> 0;
    framesCount.y = image.height / height >> 0;
  }

  public get nextFrame (): number {
    return this.framesCount.rect && ((this.currentFrame + this.animationSpeed) >> 0) % this.framesCount.rect || 0;
  }

  public render (c: CanvasRenderingContext2D, x: number, y: number): void {
    if (this.status !== SpriteStatus.LOADED) return;

    const currentFrame = this.currentFrame >> 0;

    const { frameSize, framesCount, offset, scale, angle } = this;
    const { x: width, y: height } = frameSize;

    const frameX = this.framesCount.x && (currentFrame % framesCount.x) * width || 0;
    const frameY = this.framesCount.y && ((currentFrame / framesCount.x >> 0) % framesCount.y) || 0;

    c.save();
    c.translate(x, y);
    c.scale(scale.x, scale.y);
    c.rotate(angle);
    c.globalCompositeOperation = this.blendMode;
    c.globalAlpha = this.opacity;
    c.drawImage(this.image, frameX, frameY, width, height, - offset.x * width, - offset.y * height, width, height);
    c.restore();

    if (this.framesCount.rect > 1) {
      this.currentFrame += this.animationSpeed;
    }

    if (currentFrame > this.framesCount.rect - 1) {
      this.triggerEvent('animation:end', this);
      this.currentFrame = 0;
    }
  }

  public clone (): Sprite {
    return new Sprite(this.options);
  }
}