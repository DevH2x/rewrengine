export abstract class Mouse {
  static pressed = false;
  static el: HTMLCanvasElement;
  static isOutOfContainer = false;
  static x = 0;
  static y = 0;

  static initialize(el: HTMLCanvasElement): void {
    if (el) {
      this.el = el;
      el.addEventListener('mousemove', Mouse.onMouseMove);
    }
  }

  private static onMouseMove(event: MouseEvent): void {
    const { el } = this;
    const { x: sx, y: sy, width, height } = el.getBoundingClientRect();
    
    this.x = event.pageX - sx;
    this.y = event.pageY - sy;

    this.x /= el.offsetWidth / el.width;
    this.y /= el.offsetHeight / el.height;


    this.isOutOfContainer = this.x > width || this.x < 0 || this.y > height || this.y < 0;
  }
}