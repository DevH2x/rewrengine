export abstract class Keyboard {
  static keys: number[] = [];

  static initialize (): void {
    window.addEventListener('keydown', Keyboard.onKeyDown);
    window.addEventListener('keyup', Keyboard.onKeyUp);
  }

  static isKeyPressed (key: string | number): number {
    if (typeof key === 'string') {
      key = key.charCodeAt(0);
    }
    return Keyboard.keys[key] || 0;
  }

  private static onKeyDown (event: KeyboardEvent): void {
    Keyboard.keys[event.keyCode] = 1;
  }

  private static onKeyUp (event: KeyboardEvent): void {
    Keyboard.keys[event.keyCode] = 0;
  }
}