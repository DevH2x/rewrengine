/**
 * @file Keyboard.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { EventEmitter } from "../Event";

export enum Key {
  ENTER = 13, 
  SHIFT = 16,
  CTRL = 18,
  ALT = 18,
  LEFT = 37,
  RIGHT = 39,
  UP = 38,
  DOWN = 40,
  SPACE = 32,
  ESC = 27
}

export class KeyboardInput extends EventEmitter {
  private keyboardState: number[] = [];
  private _isActive = true;
  private _keydownHandler = (...args) => this.keydownHandler.bind(this)(...args);
  private _keyupHandler = (...args) => this.keyupHandler.bind(this)(...args);

  constructor () { 
    super();
    this.initalize();
  }

  public set isActive (value: boolean) {
    if (typeof value !== 'boolean') {
      console.error('[MOUSE INPUT]: value of "isActive" should be "boolean"');
    } else {
      if (value !== this._isActive) {
        this._isActive = value;

        this[value && 'initalize' || 'deinitalize']();
      }
    }
  }

  public get isActive (): boolean {
    return this._isActive;
  }

  private keydownHandler ({ keyCode, key }) {
    this.keyboardState[keyCode] = 1;
    this.triggerEvent('keydown:' + key.toUpperCase(), this);
    this.triggerEvent('keydown:' + keyCode, this);
  }

  private keyupHandler ({ keyCode, key }) {
    this.keyboardState[keyCode] = 0;
    this.triggerEvent('keyup:' + key.toUpperCase(), this);
  }

  private initalize () {
    window.addEventListener('keydown', this._keydownHandler);
    window.addEventListener('keyup', this._keyupHandler);
  }

  private deinitalize () {
    window.removeEventListener('keydown', this._keydownHandler);
    window.removeEventListener('keyup', this._keyupHandler);

    this.keyboardState = this.keyboardState.map(() => 0);
  }

  public isKeyActive (key: string | number): number {
    if (typeof key === 'string') { 
      key = (key as string).toUpperCase().charCodeAt(0);
    }

    return this.keyboardState[key] || 0;
  }
}