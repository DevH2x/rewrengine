/**
 * @file Mouse.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { Vector2 } from "../Common";
import { EventEmitter } from "../Event";

export class MouseInput extends EventEmitter {
  public x = 0;
  public y = 0;
  public isOutOfCanvas = false;
  private _isActive = false;
  private vecPos = Vector2.zero;

  constructor (private canvas: HTMLCanvasElement) {
    super();
    this.initalize();
  }

  public set isActive (value: boolean) {
    if (typeof value !== 'boolean') {
      console.error('[MOUSE INPUT]: value of "isActive" should be "boolean"');
    } else {
      if (value !== this._isActive) {
        this._isActive = value;

        this[value && 'initalize' || 'deinitalize']();
      }
    }
  }

  public get isActive (): boolean {
    return this._isActive;
  }

  public get vector(): Vector2 {
    this.vecPos.x = this.x;
    this.vecPos.y = this.y;
    return this.vecPos;
  } 

  private initalize () {
    const { canvas } = this;

    canvas.addEventListener('mousemove', this.mouseMoveHandler.bind(this));
    canvas.addEventListener('click', this.mouseClickHandler.bind(this));
  }

  private deinitalize () {
    const { canvas } = this;

    canvas.removeEventListener('mousemove', this.mouseMoveHandler.bind(this));
    canvas.removeEventListener('click', this.mouseClickHandler.bind(this));
  }
  
  private mouseMoveHandler (event) {
    const { canvas } = this;
    const { x: sx, y: sy, width, height } = canvas.getBoundingClientRect();
    
    this.x = event.pageX - sx;
    this.y = event.pageY - sy;

    this.x /= canvas.offsetWidth / canvas.width;
    this.y /= canvas.offsetHeight / canvas.height;


    this.isOutOfCanvas = this.x > width || this.x < 0 || this.y > height || this.y < 0;
  }
  
  private mouseClickHandler () {
    this.triggerEvent('click');
  }
}