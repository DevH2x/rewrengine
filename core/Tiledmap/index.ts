/**
 * @file index.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { Debug } from "../Common";
import { ResourceManager } from "../Resources";
import { EventEmitter } from "../Event";
import { Tileset } from '../Graphics';

export interface IProperties {
  [key: string]: string;
}

export interface IPropertyDefinition {
  name: string,
  type: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  value: any
}

export interface IPropertytypes {
  [key: string]: string;
}


export interface IMapObject {
  gid?: number;
  height: number;
  id: number;
  name: string;
  properties?: IProperties | IPropertyDefinition[];
  propertytypes?: IPropertytypes;
  rotation: number;
  type: string;
  visible: boolean;
  width: number;
  x: number;
  y: number;
}

export interface ILayer {
  data?: number[];
  height?: number;
  name: string;
  opacity: number;
  type: 'tilelayer' | 'objectgroup';
  visible: boolean;
  width?: number;
  x: number;
  y: number;
  draworder?: string;
  objects?: IMapObject[];
  properties?: IProperties
}

export interface ITileset {
  columns: number;
  firstgid: number;
  image: string;
  imageheight: number;
  imagewidth: number;
  margin: number;
  name: string;
  spacing: number;
  tilecount: number;
  tileheight: number;
  tilewidth: number;
  texture: HTMLImageElement;
}

export interface IProperty {
  name: string,
  type: string
}

export interface ITiledmapData {
  height: number;
  infinite: boolean;
  layers: ILayer[];
  nextobjectid: number;
  orientation: string;
  renderorder: string;
  tiledversion: string;
  tileheight: number;
  tilesets: ITileset[];
  tilewidth: number;
  type: string;
  version: number;
  width: number;
}

export class Tiledmap extends EventEmitter {
  constructor () { 
    super(); 
  }
  
  public layers = {
    back: [] as HTMLCanvasElement[],
    front: [] as HTMLCanvasElement[]
  }

  private tilesets: Tileset[] = [];

  public data: ITiledmapData;
  
  public load (tiledmap: ITiledmapData): void {
    if (!tiledmap) return;
    this.tilesets = [];
    this.layers.back = [];
    this.layers.front = [];

    this.data = JSON.parse(JSON.stringify(tiledmap)); //Deep clone object
    this.createCanvases();
    this.loadTilesets();
    this.transformObjectProps();
    
    this.addListener('tilesets:loaded', () => {
      this.renderLayers();
      this.triggerEvent('load', this);
    }, { once: true });

    ResourceManager.registerResource(this);
  }

  private createCanvases () {
    const { data } = this;

    const { width, height, layers } = data;
    const [layerWidth, layerHeight] = [width * data.tilewidth, height * data.tileheight];
    const tiledLayers = layers.filter(layer => layer.type === 'tilelayer');
    
    const createCanvas = (layer: ILayer) => {
      const c = document.createElement('canvas');
      c.width = layerWidth;
      c.height = layerHeight;
      c.id = layer.name;

      return c;
    };

    this.layers.back = tiledLayers
      .filter(layer => !layer.properties?.front)
      .map(createCanvas);

    this.layers.front = tiledLayers
      .filter(layer => layer.properties?.front)
      .map(createCanvas);
  }

  public getObjectList (): IMapObject[] {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    /**@ts-ignore */
    return this.data.layers.filter(e => e.type === 'objectgroup').map(e => e.objects).flat();
  }

  private renderLayers () {
    const { data } = this;

    const renderLayer = canvas => {
      const c = canvas.getContext('2d');
      const layerData = data.layers.find(e => e.name === canvas.id);
      layerData.data.forEach((tileIndex, index) => {
        const x = index % this.data.width;
        const y = index / this.data.width >> 0;

        this.drawTile(tileIndex, x, y, c);
      });
    };

    this.layers.back.forEach(renderLayer);
    this.layers.front.forEach(renderLayer);
  }

  private loadTilesets() {
    let loaded = 0;
    Debug.log('Loading tilesets...');
    const increaseLoaded = () => {
      loaded++;
      if (loaded === this.data.tilesets.length) {
        this.triggerEvent('tilesets:loaded', false);
      }
    };

    this.data.tilesets.forEach((tileset: ITileset, index) => {
      const ts = new Tileset({
        url: '/gfx/' + tileset.image.split('/').pop(),
        tileWidth: tileset.tilewidth,
        tileHeight: tileset.tileheight
      });

      ts.addListener('load', increaseLoaded, { once: true });
      ts.addListener('load:cached', increaseLoaded, { once: true });
      ts.addListener('error', increaseLoaded, { once: true });


      this.tilesets[index] = ts;
    });
  }

  private drawTile(index: number, x: number, y: number, c: CanvasRenderingContext2D) {
    if (!index) return;
    const tileset = this.data.tilesets.find(tileset => index >= tileset.firstgid - 1 && index <= tileset.firstgid + tileset.tilecount - 1);
    const tilesetIndex = this.data.tilesets.indexOf(tileset);
    const trueIndex = index - tileset.firstgid;
    this.tilesets[tilesetIndex].drawTile(trueIndex, x, y, c);
  }

  private transformObjectProps () {
    const transformProps = (properties: IPropertyDefinition[] = []): IProperties => {
      const result = {};
      for (const propData of properties) {
        result[propData.name] = propData.value;
      }

      return result;
    };
    this.getObjectList().forEach(e => e.properties = transformProps(e.properties as IPropertyDefinition[]));
  }

  public render (c: CanvasRenderingContext2D, side: 'back' | 'front' = 'back'): void {
    this.layers[side].forEach(canvas => {
      if (canvas.id[0] === '$') return;

      c.drawImage(canvas, 0, 0);
    });
  }

  public getLayer(name: string): ILayer {
    return this.data.layers.find(e => e.name === name);
  }

  public getCanvasByLayerName(name: string): HTMLCanvasElement {
    return this.layers.back.find(e => e.id === name) || this.layers.front.find(e => e.id === name);
  }
}

export * from './CollisionMap';
export * from './LightMap';