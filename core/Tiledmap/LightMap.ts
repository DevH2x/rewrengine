/**
 * @file LightMap.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { Color } from "@core";
import { EventEmitter } from "@core";
import { CollisionMap } from "./CollisionMap";

export interface ILightMapOptions {
  ambient?: string;
  softness?: number;
}

export class LightMap extends EventEmitter {

  public texture: CanvasRenderingContext2D = document.createElement('canvas').getContext('2d')

  /**
   * Create lightmap texture
   * @param collisionMap 
   */
  constructor (
    private collisionMap: CollisionMap,
    private options: ILightMapOptions = {}
  ) 
  {
    super();
    this.reset();
  }

  /**
   * Draws point light to lightmap texture
   * @param x 
   * @param y 
   * @param radius 
   * @param color 
   * @param rays 
   */
  public pointLight(x: number, y: number, radius: number, color: string, rays = 360): void {
    const { texture: c } = this;
    const angle = (Math.PI * 2) / rays;
    const colorInstance = new Color();
    colorInstance.fromHEXWithAlpha(color); 
    
    c.fillStyle = c.createRadialGradient(x, y, 0, x, y, radius);
    c.fillStyle.addColorStop(0, colorInstance.toStyleString());
    colorInstance.a = 0;
    c.fillStyle.addColorStop(1, colorInstance.toStyleString());

    c.beginPath();
    for (let i = 0; i < rays; i++) {
      const point = this.shootRay(x, y, angle * i, radius);
      c[!i && 'moveTo' || 'lineTo'](...point);
    }
    c.globalCompositeOperation = "color-dodge";
    c.globalAlpha = .7;
    c.fill();
  }

  /**
   * Returns position where ray was collided
   * @param x in px
   * @param y in px
   * @param direction in rad 
   */
  private shootRay (x: number, y: number, direction: number, maxLength = 30): [number, number] {
    const { sin, cos } = Math;

    let rx: number, ry: number;

    for (let i = 0; i < maxLength; i++) {
      rx = (x + cos(direction) * i) >> 0;
      ry = (y + sin(direction) * i) >> 0;

      if (this.collisionMap.checkCollision(rx, ry)) {
        break;
      }
    }

    return [rx, ry];
  }

  public reset (): void {
    const { texture: c } = this;
    const filterList = [];
    c.canvas.width = this.collisionMap.canvas.width;
    c.canvas.height = this.collisionMap.canvas.height;

    c.fillStyle = this.options.ambient || '#000';
    c.fillRect(0, 0, c.canvas.width, c.canvas.height);

    if (this.options.softness) {
      filterList.push(`blur(${this.options.softness}px)`);
    }

    filterList.push('saturate(400%)');

    c.filter = filterList.join(' ');
  }

  public render (c: CanvasRenderingContext2D, blendMode = "color-dodge"): void {
    c.save();
    c.globalCompositeOperation = "multiply";
    c.drawImage(this.texture.canvas, 0, 0);
    c.globalCompositeOperation = blendMode;
    c.drawImage(this.texture.canvas, 0, 0);
    c.restore();
  }
}