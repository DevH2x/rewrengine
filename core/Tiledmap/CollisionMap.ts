/**
 * @file CollisionMap.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { EventEmitter } from "@core";
import { rendererInstance } from "game/engine/renderer";

export class CollisionMap extends EventEmitter {
  private collisionData: Uint8ClampedArray

  constructor (
    public canvas: HTMLCanvasElement
  ) { super(); this.generateCollisionData(); }

  private generateCollisionData () {
    this.collisionData = this.canvas
      .getContext('2d')
      .getImageData(0, 0, this.canvas.width, this.canvas.height).data
      .filter((e, i) => i % 4 === 0);
  }

  public checkCollision (x: number, y: number): boolean {
    if (!this.canvas) return false;
    x = x >> 0;
    y = y >> 0;

    const pixelIndex = y * this.canvas.width + x;
    return this.collisionData[pixelIndex] !== undefined && this.collisionData[pixelIndex] > 0;
  }


  /**
   * Debug Rendering collision mask
   */
  public renderCollisionData(c: CanvasRenderingContext2D) {

    c.fillStyle = "#f00";
    c.beginPath();
    this.collisionData.forEach((e, i) => {
      const x = i % this.canvas.width;
      const y = i / this.canvas.width >> 0;
      if (e > 0) {
        c.rect(x, y, 1, 1);
      }
    });

    c.fill();
  }
}