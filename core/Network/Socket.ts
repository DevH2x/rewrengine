import { Actor } from "../Actor";
import { EventEmitter } from "../Event";
import { WebSocketEvents } from "./Events";

export class Socket extends EventEmitter {
  private socket: WebSocket;

  constructor(url: string) {
    super();
    this.socket = new WebSocket(url);
    this.initEventListeners();
  }

  public get isConnected (): boolean {
    return this.socket.readyState === this.socket.OPEN;
  }

  private initEventListeners(): void {
    this.socket.addEventListener('open', (event) => this.triggerEvent('open', event));
    this.socket.addEventListener('message', (event) => this.triggerEvent('message', event));
    this.socket.addEventListener('close', (event) => this.triggerEvent('close', event));
    this.socket.addEventListener('error', (error) => this.triggerEvent('error', error));
  }

  public sendActorState (actor: Actor): void {
    const { position: { array: position }, angle, opacity, sprite, scale: { array: scale } } = actor;
    const data = [WebSocketEvents.ACTOR_STATE, ...position, ...scale, angle, opacity, sprite].join('$$');
    
    this.socket.send(data);
  }
}