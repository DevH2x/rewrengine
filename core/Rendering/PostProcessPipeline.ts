import { EventEmitter } from "events";
import { ShaderCanvas } from "shader-canvas";

interface IPostProcessPipe<T> {
  shader: ShaderCanvas,
  _priority: number,
  priority?: number,
  uniforms: T,
  beforeUpdate?: (s: ShaderCanvas) => void
}

export class PostProcessPipeline extends EventEmitter {
  private pipeline: IPostProcessPipe<any>[] = [];

  constructor (private canvas: HTMLCanvasElement) {
    super();
  }

  /**
   * Add processing to pipeline
   * @param source GLSL Fragment Shader Source
   * @param priority render priorinty (0 -> higher priority index)
   * @param beforeUpdate calls before rendering
   */
  public addShader<T>(source: string, priority: number, beforeUpdate?: (s: ShaderCanvas) => void): IPostProcessPipe<T> {
    const { width, height } = this.canvas;
    const shader = new ShaderCanvas();
    const ppPipe: IPostProcessPipe<T> = {
      shader,
      uniforms: this.createUniformsProxy(source, shader),
      _priority: priority,
      beforeUpdate
    };

    shader.setSize(width, height);
    shader.setShader(source);
    shader.setUniform('u_resolution', [
      width * window.devicePixelRatio,
      height * window.devicePixelRatio,
    ]);

    shader.testUniform('u_pixelRatio') &&
      shader.setUniform('u_pixelRatio', window.devicePixelRatio);

    return this.createPipe(ppPipe);
  }

  public render(time: number): void {
    this.pipeline.reduce((prev, curr) => {
      const canvas: HTMLCanvasElement = !prev ? this.canvas : prev.shader.domElement;

      curr.shader.domElement.style.imageRendering = canvas.style.imageRendering;

      curr.shader.setTexture('u_canvas', canvas as any);
      
      curr.shader.testUniform('u_time') &&
        curr.shader.setUniform('u_time', time);
      
      curr.beforeUpdate?.(curr.shader);
      curr.shader.render();

      return curr;
    }, false as any);
  }

  private createUniformsProxy<T>(source: string, shader: ShaderCanvas): T {
    const regex = /uniform ([a-z0-9]+)\s([a-z_]+);/i;
    const proxy = {} as T;
    const excludeUniforms = ['u_canvas', 'u_resolution', 'u_time'];

    source
      .split('\n')
      .forEach(uniformDefinitionString => {
        if (!regex.test(uniformDefinitionString)) return;

        const [uniform, type, name] = uniformDefinitionString.match(regex);

        if (excludeUniforms.includes(name)) return;

        if (type === 'float' || type === 'int') {
          proxy[name.replace('u_', '')] = 0;
        }
      });

    return new Proxy<any>(proxy, {
      set (t, n: string, v: any) {
        if (shader.testUniform('u_' + n)) {
          shader.setUniform('u_' + n, v);
          t[n] = v;
          return true;
        }

        return false;
      }
    });
  }

  private createPipe<T>(pipe: IPostProcessPipe<T>): IPostProcessPipe<T> {
    const sort = this.sortPipeline.bind(this);
    Object.defineProperty(pipe, 'priority', {
      get () {
        return this._priority;
      },

      set (value) {
        if (value !== this._priority) {
          this._priority = value;
          sort();
        }
      }
    });

    this.pipeline.push(pipe);
    this.sortPipeline();

    return pipe;
  }

  private sortPipeline () {
    this.pipeline.sort((a, b) => {
      return b.priority - a.priority;
    });
  }

  public get outputCanvas (): HTMLCanvasElement {
    return this.pipeline[this.pipeline.length - 1].shader.domElement;
  }
}