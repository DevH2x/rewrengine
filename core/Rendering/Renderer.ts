

import { Debug } from "../Common";
import { EventEmitter } from "../Event";
export interface RendererOptions {
  width?: number;
  height?: number;
  background?: string;

  /**
   * id attr of `HTMLCanvasElement`
   */
  canvasId?: string;

  /**
   * Where canvas will be placed in DOM tree
   * @default "body"
   */
  selector?: string;

  pixelatedRendering?: boolean;

  /**
   * Canvas scale
   */
  scale?: number;

  preventAppend?: boolean;
}
export interface RenderMethodOptions {
  once?: boolean
}

/**
 * Рендер-функция, которая будет вызываться при вызове метода render объекта класса Renderer
 * @param { CanvasRenderingContext2D } Context - Контекст отрисовки
 * @param { number } deltaTime - Время прошедше между текущим и предыдущим кадром (ms)
 * @param { number } fps - Количество кадров отрисованных за секунду
 */
export type RenderFunction = (Context: CanvasRenderingContext2D, deltaTime?: number, fps?: number, timestamp?: number) => void | false;

export class Renderer extends EventEmitter {
  public Canvas: HTMLCanvasElement = document.createElement('canvas');
  public Context: CanvasRenderingContext2D = this.Canvas.getContext('2d', { alpha: false });
  private Options: RendererOptions = {
    width: 800,
    height: 600,
    selector: 'body',
    canvasId: 'RewrengineCanvas',
    background: '#000',
    pixelatedRendering: true,
    scale: 1,
    preventAppend: false
  }
  
  constructor (Options: RendererOptions = {}) {
    super();
    this.addListener('config:changed', this.initCanvas);
    this.setConfig(Options);
  }

  /**
   * Обчаивает дефолтную калку с заданным чайком
   * @param { RendererOptions } Options Настройки рендеринга
   */
  public setConfig (Options: RendererOptions): void {
    this.Options = {
      ...this.Options,
      ...Options
    };
    this.triggerEvent('config:changed', this.Options);
  }

  private initCanvas (): void {
    const { width, height, selector, canvasId, pixelatedRendering, scale } = this.Options;
    const parentElement = document.querySelector(selector);
    this.Canvas.width = width;
    this.Canvas.height = height;

    this.Context.imageSmoothingEnabled = !pixelatedRendering;
    this.Canvas.style.imageRendering = pixelatedRendering && 'pixelated' || '';

    this.Canvas.style.width = (width * scale) + 'px';
    this.Canvas.style.height = (height * scale) + 'px';

    if (!this.Options.preventAppend) {
      if (!parentElement.contains(this.Canvas)){
        parentElement.appendChild(this.Canvas);
        this.Canvas.id = canvasId;
      }
    }
  }


  /**
   * Запускает рендер функцию для отрисовки на холсте. 
   * - Если рендер-функция возвращает false, то функция останавливает свою работу
   * @param { RenderFunction } f - функция рендеринга
   * @param { RenderMethodOptions } options - опции рендеринга 
   */
  public async render (f:RenderFunction, options?: RenderMethodOptions): Promise<void> {
    let OLD_TIME_STAMP = 0;

    const renderLoop = (TIME_STAMP) => {
      let stopRendering: boolean;
      const thresholdTimeToSkip = 0.2;
      const DELTA_TIME = (TIME_STAMP - OLD_TIME_STAMP) / 1000;
      const FPS = 1 / DELTA_TIME;
      

      OLD_TIME_STAMP = TIME_STAMP;
      if (DELTA_TIME < thresholdTimeToSkip) {
        this.triggerEvent('render:start', TIME_STAMP);
        stopRendering = f(this.Context, DELTA_TIME, FPS, TIME_STAMP) === false;
        this.triggerEvent('render:end');
      } else {
        Debug.log('Frame skipped');
      }
      
      if (!stopRendering || !options?.once) {
        requestAnimationFrame(renderLoop);
      }
    };

    requestAnimationFrame(renderLoop);
  }
}