/**
 * @file index.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

/**
 * Event Listener Options
 */
export interface EventListenerOptions {
  /**
   * Removes the listener after the trigger
   */
  once?: boolean,
  /**
   * Removes the listener after the trigger a certain number of times
   */
  takes?: number
}

export type EventHandler = (details: any) => void;

export interface EventListener {
  handler: EventHandler,
  options: EventListenerOptions,
  event: string;
}

export interface EventEmitterTypes {
  [eventName: string ]: EventListener[]
}

export class EventEmitter {
  private events: EventEmitterTypes = {}

  /**
   * 
   * @param event Event type
   * @param handler listener
   * @param options options
   */
  public addListener(event: string, handler: EventHandler, options: EventListenerOptions = {}): () => void {
    if (!this.isEventTypeDefined(event)) {
      this.defineEventType(event);
    }

    const eventListener: EventListener = {
      handler,
      options,
      event
    };

    this.events[event].push(eventListener);

    return () => this.removeListener(event, eventListener.handler);
  }

  public triggerEvent(eventName: string, details?: any): void {
    if (!this.isEventTypeDefined(eventName)) {
      return;
    }
    this.events[eventName].forEach(eventListener => {
      eventListener.handler.bind(this)(details);
    });
    this.events[eventName].forEach(this.checkEventListener.bind(this));
  }

  private checkEventListener(eventListener: EventListener): void {
    const { event: eventName } = eventListener;
    if (eventListener.options.once) {
      this.removeListener(eventName, eventListener.handler);
    }

    if (typeof eventListener.options.takes === 'number') {
      eventListener.options.takes--;
      if (eventListener.options.takes === 0) {
        this.removeListener(eventName, eventListener.handler);
      }
    }
  }

  public removeListener(event: string, handler: EventHandler): void {
    this.events[event] = this.events[event].filter(e => e.handler !== handler);

    if (!this.events[event].length) {
      delete this.events[event];
    }
  }

  private isEventTypeDefined(eventName: string): boolean {
    return !!this.events[eventName];
  }

  private defineEventType(eventName: string): void {
    this.events[eventName] = [];
  }
}