/**
 * @file index.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { Sprite } from "..";
import { BorderBox, Vector2 } from "../Common";
import { EventEmitter } from "../Event";
import { Scene } from "../Scene";

export interface SpriteState {
  [key: string]: Sprite
}
export interface Actor {
  beforeUpdate?(c?: CanvasRenderingContext2D, dt?: number): void,
  afterUpdate?(c?: CanvasRenderingContext2D, dt?: number): void,
}
export abstract class Actor extends EventEmitter {

  /** World position of actor */
  public readonly position = new Vector2(0, 0);

  /** Current Sprite state */
  public sprite = 'default';

  /** Scale of current sprite */
  public scale = new Vector2(1, 1);

  /** Angle of current sprite */
  public angle = 0;

  /** Opacity of current sprite */
  public opacity = 1;

  /** Spritelist of the Actor */
  public spriteList: SpriteState = {};

  /** Update priority of the Actor */
  public depth = 0;
  
  /** Scene instance where the Actor was spawned */
  protected scene: Scene | any;

  /** Collision box of the Actor */
  public bbox: BorderBox = new BorderBox(0, 0, 0, 0);

  public refName: string | number;

  constructor (public properties = {}) {
    super();
    this.addListener('spawn', ({ scene }) => this.scene = scene);
    this.bbox.setActor(this);
  }

  public get currentSprite (): Sprite {
    return this.spriteList[this.sprite];
  }

  public get isInViewport (): boolean {
    const { scene } = this;

    if (!scene) return false;
    
    const { viewport } = scene;
    const { x, y } = this.position;

    if (!this.bbox.width || !this.bbox.height) {
      return ( 
        x > viewport.x - viewport.width / 2 &&
        x < viewport.x + viewport.width / 2 &&
        y > viewport.y - viewport.height / 2 &&
        y < viewport.y + viewport.height / 2
      );
    } else {
      return ( 
        this.bbox.right > viewport.x - viewport.width / 2 &&
        this.bbox.left < viewport.x + viewport.width / 2 &&
        this.bbox.bottom > viewport.y - viewport.height / 2 &&
        this.bbox.top < viewport.y + viewport.height / 2
      );
    }
  }

  public update (c?: CanvasRenderingContext2D, dt?: number): void {
    if (this.currentSprite && this.isInViewport) {
      this.currentSprite.scale.x = this.scale.x;
      this.currentSprite.scale.y = this.scale.y;
      this.currentSprite.angle = this.angle;
      this.currentSprite.opacity = this.opacity;
      this.currentSprite.render(c, this.position.x, this.position.y);    
    }
  }

  public destroy (): void {
    this.scene.destroyChild?.(this);
  }
}