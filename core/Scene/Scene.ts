/**
 * @file Scene.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { IRenderPipeline } from "core/Interfaces";
import { EventEmitter } from "..";
import { Actor } from "../Actor";

const UPDATE_CYCLE = ['beforeUpdate', 'update', 'afterUpdate'];

interface ViewportSettings {
  x: number,
  y: number,
  width: number,
  height: number,
  zoom: number
}
export interface Scene extends IRenderPipeline {
  staticForeground?(c: CanvasRenderingContext2D, dt?: number, fps?: number, t?: number): void,
  staticBackground?(c: CanvasRenderingContext2D, dt?: number, fps?: number, t?: number): void,
}
export class Scene extends EventEmitter {
  public objects: Actor[] = [];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public props: { [key: string]: any } = {};
  public viewport: ViewportSettings = {
    x: 0,
    y: 0,
    width: 640,
    height: 480,
    zoom: 1
  }
  public name = 'noname';

  private _saved_scene_props = {}

  public load (): void {
    this.savePropsState();
  }

  public clear (): void {
    this.destroyObjects();
    this.resetPropsState();
  }

  public savePropsState (): void {
    this._saved_scene_props = {
      ...this.props
    };
  }

  public resetPropsState (): void {
    this.props = {
      ...this._saved_scene_props
    };
  }

  public destroyObjects (): void {
    this.objects.forEach(e => e.triggerEvent('destroy'));
    this.objects = [];
  }

  private updateObjects (c: CanvasRenderingContext2D, d?: number, f?: number, t?: number, method?: string) {
    this.objects.forEach(object => object[method]?.(c, d, f));
  }

  public render (c: CanvasRenderingContext2D, d?: number, f?: number, t?: number): void {
    const { x, y, width, height, zoom } = this.viewport;
    const scaleX = c.canvas.width / width * zoom;
    const scaleY = c.canvas.height / height * zoom;

    this.staticBackground?.(c, d, f, t);
    c.save();
    c.scale(scaleX, scaleY);
    c.translate((-x + width / zoom / 2) >> 0, (-y + height / zoom / 2) >> 0);

    UPDATE_CYCLE.forEach(method => {
      this[method]?.(c, d, f, t);
      this.updateObjects(c, d, f, t, method);
    });

    c.restore();
    this.staticForeground?.(c, d, f, t);
  }

  public setViewportSettingsToContext(c: CanvasRenderingContext2D): void {
    const { x, y, width, height, zoom } = this.viewport;
    const scaleX = c.canvas.width / width * zoom;
    const scaleY = c.canvas.height / height * zoom;
    
    c.scale(scaleX, scaleY);
    c.translate((-x + width / zoom / 2) >> 0, (-y + height / zoom / 2) >> 0);
  }

  protected addChild (object: Actor): void {
    this.objects.push(object);
    this.objects.sort((a, b) => b.depth - a.depth);
    object.triggerEvent('addedtoscene', { scene: this });
  }

  public destroyChild (object: Actor): void {
    this.objects.splice(this.objects.indexOf(object), 1);
  }
}