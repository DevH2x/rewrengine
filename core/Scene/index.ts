/**
 * @file index.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { EventEmitter } from "../Event";
import { BlankScene } from "./BlankScene";
import { Scene } from './Scene';

export class SceneDispatcher extends EventEmitter {
  private blankScene = new BlankScene;
  private currentScene: Scene = this.blankScene;

  public load (scene: Scene = this.blankScene): void {
    this.currentScene?.triggerEvent('leave');
    this.currentScene = scene;
    this.currentScene.load();
    this.triggerEvent('scene:load', scene);
  }

  public render (context?: CanvasRenderingContext2D, dt?: number, fps?: number, timestamp?: number): void {
    const { currentScene } = this;

    if (currentScene) {
      currentScene.render(context, dt, fps, timestamp);
    }
  }
}
export { Scene };