/**
 * @file index.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { MouseInput } from "core/Input";
import { UIElement } from "./UIElement";


export class UI {
  static components: { [key: string]: any } = {};
  static mouse: MouseInput;
  static SystemCall: { [key: string]: any } = {};
  static isMouseClicked = false;

  static parse (HTMLString: string): UIElement {
    const container = document.createElement('div');
    container.innerHTML = HTMLString;

    const createElement = (element: Element, root?): UIElement => {
      const { childNodes: children, localName } = element;
      const properties = {};
      
      const componentName = localName.replace(/(-[a-z]|^[a-z])/ig, (x) => {
        return x.replace('-', '').toUpperCase();
      });

      element.getAttributeNames().forEach(attributeName => {
        properties[attributeName] = element.getAttribute(attributeName);
      });
      const Component = UI.components[componentName] || UIElement;
      const uiElement = new Component(properties) as UIElement;
      
      root = root || uiElement;

      for (let i = 0; i < children.length; i++) {
        if (children[i].nodeName === '#text') {
          uiElement.text.push(children[i].textContent);
        } else {
          const childElement = createElement(children[i] as Element, root);
          childElement.root = root;
          uiElement.add(childElement);
        }
      }

      return uiElement;
    };

    return createElement(container.children[0]);
  }

  static registerComponent(componentName: string, componentClass): void {
    this.components[componentName] = componentClass;
  }
}

export * from './UIElement';