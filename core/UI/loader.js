module.exports = function (source) {
  return `
    import { UI } from '@core'
    export default UI.parse(\`${source}\`);
  `;
};