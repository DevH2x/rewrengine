import { BorderBox, EventEmitter } from "../";
import { UI } from "./";

export interface IUIElementDefaultProps {
  x?: number | string,
  y?: number | string,
  width?: number | string,
  height?: number | string,
  [key: string]: any
}

export class UIElement extends EventEmitter {
  public bbox: BorderBox = new BorderBox(0, 0, 300, 300);
  public children: UIElement[] = [];
  public parent: UIElement;
  public text: string[] = [];
  public root: UIElement;
  
  private _isActive = false;
  private _isFirstRender = true;
  private _hoverState = false;

  constructor (
    public properties: IUIElementDefaultProps
  ) { 
    super ();

    ['x', 'y', 'width', 'height'].forEach(key => {
      if (properties[key]) {
        this.bbox.change({
          [key]: parseInt(this.properties[key])
        });
      }
    });
  }

  public initClickEvents (): void {
    UI.mouse?.addListener('click', () => {
      const allowClick = this.root ? this.isActive && this.root.isActive : this.isActive;
      if (allowClick && this.isHover) {
        if (!UI.isMouseClicked) {
          this.triggerEvent('click');
          UI.isMouseClicked = true;
        }

        setTimeout(() => UI.isMouseClicked = false);
      }
    });
  }

  public get isRoot (): boolean {
    return this.root === this;
  }

  public get isActive (): boolean {
    return this._isActive;
  }

  public set isActive (value: boolean) {
    this.children.forEach(child => child.isActive = value);
    this._isActive = value;

    if (!value)
      this._isFirstRender = true;
  }

  protected get SystemCall (): any {
    return UI.SystemCall;
  }

  public getAbsolutePosition (): [number, number] {
    return [
      (this.parent?.bbox.x || 0) + this.bbox.x,
      (this.parent?.bbox.y || 0) + this.bbox.y,
    ];
  }

  protected getRelativeMousePosition (): [number, number] {
    const [x, y] = this.getAbsolutePosition();
    return [UI.mouse?.x - x, UI.mouse?.y - y];
  }

  protected set hoverState (value: boolean) {
    if (value !== this._hoverState) {
      this.triggerEvent('hover:changed', value);
      this._hoverState = value;
    }
  }

  protected get hoverState(): boolean {
    return this._hoverState;
  }

  protected get isHover (): boolean {
    const [x, y] = this.getRelativeMousePosition();
    this.hoverState = x > 0 && x < this.bbox.width && y > 0 && y < this.bbox.height;

    return this.hoverState; 
  }

  public visualize (c: CanvasRenderingContext2D): void {
    c.strokeStyle = '#f00';
    c.strokeRect(0, 0, this.bbox.width, this.bbox.height);
  }

  public render (c: CanvasRenderingContext2D): void {
    if (!this.isActive) return;

    if (this._isFirstRender) {
      this.children.forEach(e => e['beforeRender']?.(c));
    }

    c.save();
    c.translate(this.bbox.x, this.bbox.y);
    this.visualize(c);
    this.children.forEach(e => {
      e.isActive = this.isActive;
      e.render(c);
    });
    c.restore();

    if (this._isFirstRender) {
      this.children.forEach(e => e['rendered']?.(c));
    }

    this._isFirstRender = false;
  }

  public add (component: UIElement): void {
    this.children.push(component);
    component.parent = this;
    component['created']?.();
  }

  private getVariables (_this = 'this') {
    const variables = [];

    for (const key in this) {
      variables.push(`${key}=${_this}['${key}']`);
    }

    return variables.join(',\n');
  }

  public execute (script: string): any {
    const variables = this.getVariables();

    const totalScript = `const ${variables};
      ${script};
    `;

    return eval(totalScript);
  }

  public parseText (str: string): string {
    const expressionRegex = /\{\{\s+?\n?[\s+]?(.*?)\n?\s+?\}\}/g;
    if (!str.match(expressionRegex)) return str;

    //eslint-disable-next-line
    const _this = this;
    const { SystemCall } = this;
    const variables = this.getVariables('_this');
    return str.replace(expressionRegex, (x, expression) => {
      const evalScript = `
        const ${variables};
        ${expression.trim()}
      `;
      const result = eval(evalScript);

      return result === undefined ? '' : result;
    });
  }
}