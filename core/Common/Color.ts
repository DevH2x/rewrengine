/**
 * @file Color.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

export class Color {
  constructor (
    public r: number = 0,
    public g: number = 0,
    public b: number = 0,
    public a: number = 1
  ) {}

  public toStyleString (): string {
    const { r, g, b, a } = this;
    return `rgba(${r}, ${g}, ${b}, ${a})`;
  }

  public toHex (): string {
    let r = this.r.toString(16);
    let g = this.r.toString(16);
    let b = this.r.toString(16);

    r = r.length < 2 && '0' + r || r;
    g = g.length < 2 && '0' + g || g;
    b = b.length < 2 && '0' + b || b;

    return '#'+r+g+b;
  }

  public fromHEX (string: string): string {
    if (string.length < 7) {
      console.warn('[COLOR]: Invalid HEX data');
      return;
    }
    string = string.replace('#', '');
    const r = string.substr(0, 2);
    const g = string.substr(2, 2);
    const b = string.substr(4, 2);

    this.r = parseInt(r, 16);
    this.g = parseInt(g, 16);
    this.b = parseInt(b, 16);
  }

  public fromHEXWithAlpha (string: string): void {
    if (string.length < 9) {
      console.warn('[COLOR]: Invalid HEX data');
    }

    const rgbHex = string.substr(0, 7);
    
    this.fromHEX(rgbHex);
    this.a = parseInt(string.substr(7, 2), 16);
  }
}