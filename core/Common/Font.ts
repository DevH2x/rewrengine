import { EventEmitter } from "../Event";
import { ResourceManager } from "../Resources";

export class GFont extends EventEmitter {
  constructor (private name: string, private url: string) {
    super();

    this.loadFont();
  }

  private loadFont() {
    const style = document.createElement('style');
    const div = document.createElement('div');

    div.innerHTML = ' ';
    div.style.fontFamily = this.name;
    document.body.insertBefore(div, document.body.children[0]);
    ResourceManager.registerResource(this);
    fetch(this.url)
      .then(type => type.blob())
      .then(blob => {
        const fr = new FileReader();
        fr.readAsDataURL(blob);
        fr.onload = ({ target: { result: base64 }}) => {
          style.innerHTML = `
            @font-face {
              font-family: '${this.name}';
              src: url(${base64});
            }
          `;
          document.head.appendChild(style);
          
          setTimeout(() => this.triggerEvent('load', this), 1000);
        };
      });
  }
}
