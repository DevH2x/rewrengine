const log = (...args) => console.log('[ENGINE LOG]: ', ...args);
const error = (...args) => console.error('[ENGINE ERROR]: ', ...args);
const warn = (...args) => console.warn('[ENGINE WARNING]: ', ...args);

export const Debug = { log, error, warn };