/**
 * @file BorderBox.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { Actor } from "../Actor";
import { Vector2 } from "./Vector2";

interface BorderBoxOptions {
  x?: number,
  y?: number,
  width?: number,
  height?: number,
}

export class BorderBox {

  private _center = Vector2.zero;

  /**
   * Create border box object
   * 
   * @param oX pivot X
   * @param oY pivot Y
   * @param width width of box
   * @param height height of box
   */
  constructor (
    public x: number,
    public y: number,
    public width: number,
    public height: number,
    public actor?: Actor
  ) {}


  /**
   * Changes one or more params of BorderBox instance
   * @param borderBoxOptions border box parameters
   */
  public change (borderBoxOptions: BorderBoxOptions): BorderBox {
    for (const key in borderBoxOptions) {
      this[key] = borderBoxOptions[key];
    }
    return this;
  }


  /**
   * Set main Actor
   * @param actor Actor to which the box will be attached
   */
  public setActor (actor: Actor): BorderBox {
    this.actor = actor;
    return this;
  }

  public render (c: CanvasRenderingContext2D): void {
    const { x, y, width, height, actor } = this;
    c.strokeStyle = '#0f0';

    if (actor) {
      const { position } = actor;
      c.strokeRect(position.x - x, position.y - y, width, height);
    } else {
      c.strokeRect(x, y, width, height);
    }
  }

  public get bottom (): number {
    const { y, height, actor } = this;
    const { position } = actor;

    return position.y + height - y;
  }

  public get top (): number {
    const { y, actor } = this;
    const { position } = actor;

    return position.y - y;
  }

  public get left (): number {
    const { x, actor } = this;
    const { position } = actor;

    return position.x - x;
  }

  public get right (): number {
    const { x, width, actor } = this;
    const { position } = actor;

    return position.x + width - x;
  }

  public get center(): Vector2 {
    const { width, height, x, y, actor, _center: center } = this;
    const { position } = actor;

    center.x = (position.x + width / 2) - x;
    center.y = (position.y + height / 2) - y;

    return center;
  }

  public get array (): number[] {
    return [this.x, this.y, this.width, this.height];
  }

  public isPointInside(point: Vector2): boolean {
    return (
      point.x > this.left && point.x < this.right &&
      point.y > this.top && point.y < this.bottom
    );
  }

  public getRelativePosition(position: Vector2): Vector2 {
    return new Vector2(
      (position.x - this.left),
      (position.y - this.top)
    );
  }
}