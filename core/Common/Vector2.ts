/**
 * @file Vector2.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

export class Vector2 {
  private normalizedVector: Vector2;

  constructor (
    public x: number, 
    public y: number
  ) {}

  public clone (): Vector2 {
    return new Vector2 (this.x, this.y);
  }

  public additive (vector: Vector2): Vector2 {
    return new Vector2(this.x + vector.x, this.y + vector.y);
  }

  public add(vector: Vector2): Vector2 {
    this.x += vector.x;
    this.y += vector.y;
    return this;
  }

  public sub(vector: Vector2): Vector2 {
    this.x -= vector.x;
    this.y -= vector.y;
    return this;
  }

  public substract (vector: Vector2): Vector2 {
    return new Vector2(this.x - vector.x, this.y - vector.y);
  }

  public multiple (vector: Vector2): Vector2 {
    this.x = this.x * vector.x;
    this.y = this.y * vector.y;
    return this;
  }

  public multipleBy (n: number): Vector2 {
    return new Vector2(this.x * n, this.y * n);
  }

  public set(x: number | Vector2, y?: number): Vector2 {
    if (!x) return;
    if (typeof x === 'number') {
      this.x = x;
      this.y = y;
    } else {
      this.x = x.x;
      this.y = x.y;
    }

    return this;
  }

  public toString (): string {
    return `x: ${this.x.toFixed(2)}; y: ${this.y.toFixed(2)}`;
  }

  public distanceV(vector: Vector2): Vector2 {
    return new Vector2(vector.x - this.x, vector.y - this.y);
  }

  public distance(vector: Vector2): number {
    return Math.sqrt((vector.x - this.x) ** 2 + (vector.y - this.y) ** 2);
  }

  public angleBetween(vector: Vector2): number {
    const dx = vector.x - this.x;
    const dy = vector.y - this.y;

    return Math.atan2(dy, dx);
  }

  static fromDirectionAndScale(dir: number, scale: number): Vector2 {
    return new Vector2(Math.cos(dir) * scale, Math.sin(dir) * scale);
  }
  
  public get magnitude (): number {
    return Math.sqrt(this.x ** 2 + this.y ** 2);
  }


  public get array (): [number, number] {
    return [this.x, this.y];
  }

  public get rect (): number {
    return this.x * this.y;
  }

  public get angle (): number {
    return -Math.atan2(this.y, this.x);
  }

  public get normalized (): Vector2 {
    if (!this.normalizedVector) {
      this.normalizedVector = new Vector2(this.x / this.magnitude, this.y / this.magnitude);
    }
    this.normalizedVector.x = this.x / this.magnitude;
    this.normalizedVector.y = this.y / this.magnitude;

    return this.normalizedVector;
  }

  static get zero(): Vector2 {
    return new Vector2(0, 0);
  }
}