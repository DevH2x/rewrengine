import { EventEmitter } from "../Event";
import { Debug } from '../Common';

enum EResourceManagerState {
  IDLE,
  LOADING,
  ERROR
}

export abstract class ResourceManager {
  static events = new EventEmitter();
  static status = EResourceManagerState.IDLE;
  
  private static filesLoaded = 0;
  private static filesTotal = 0;

  static registerResource (resource: any | EventEmitter): void {

    (resource as EventEmitter).addListener('error', () => {
      ResourceManager.events.triggerEvent('error');
      ResourceManager.events.triggerEvent('resource:error', resource);
      ResourceManager.status = EResourceManagerState.ERROR;

      // eslint-disable-next-line no-use-before-define
      destroyLoadListener();
    }, { once: true });
    const destroyLoadListener = resource.addListener('load', () => {
      ResourceManager.events.triggerEvent('resource:loaded', resource);
      ResourceManager.filesLoaded++;

      ResourceManager.checkLoadingState();
    }, { once: true });


    ResourceManager.events.triggerEvent('resource:added', resource);
    ResourceManager.filesTotal++;

    ResourceManager.status = EResourceManagerState.LOADING;

    Debug.log('[RESOURCE ADDED]', resource);
  }

  static get loaded (): number {
    if (ResourceManager.filesTotal === 0) {
      return 1;
    }
    return ResourceManager.filesLoaded / ResourceManager.filesTotal;
  }

  static get percentLoaded (): number {
    return (ResourceManager.loaded * 100) >> 0;
  }

  static get isLoaded (): boolean {
    return this.loaded === 1;
  }

  private static checkLoadingState (): void {
    if (this.isLoaded) {
      setTimeout(() => {
        if (this.isLoaded) {
          ResourceManager.status = EResourceManagerState.IDLE;
          ResourceManager.events.triggerEvent('load');
        }
      }, 1000);
    }
  }
}