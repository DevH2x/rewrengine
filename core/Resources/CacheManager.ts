import { Debug } from "@core";

export abstract class CacheManager {
  static data: { [key: string]: any } = {}
  static add(url: string, result: any): void {
    Debug.log('[CACHED]: ', url);
    this.data[url] = result;
  }

  static get(url: string): any {
    this.data[url] && Debug.log('[GET CACHED]: ', url);
    return this.data[url];
  }
}