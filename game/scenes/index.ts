const ResolveScene = require.context('./', false, /\.ts$/);
const Scenes: { [key: string]: any } = {};
ResolveScene.keys().forEach(path => {
  if (path.includes('index.ts')) return;
  const SceneName = path.split('/').pop().split('.')[0];
  const Module = ResolveScene(path);
  const SceneClass = Module[Object.keys(Module)[0]];
  Scenes[SceneName] = new SceneClass;
  Scenes[SceneName].name = SceneName;
});


export { Scenes };
