import { LightMap, ILightMapOptions } from "@core";
import { PointLightProps } from "game/objects/PropsTypes";

export function initLighting (lightmapOptions: ILightMapOptions = {
  ambient: '#0e301e',
  softness: 3
}): void {

  if (!this.mapWorker) {
    throw new Error('mapWorker in current scene isn\'t instance of TiledMap or doesn\'t exsist');
  }

  const objectList = this.mapWorker.getObjectList();

  this.lightmap = new LightMap(this.collisionMap, lightmapOptions);
  this.lightmapPulsed = new LightMap(this.collisionMap, lightmapOptions);

  const pointLights = objectList
    .filter(e => e.type === 'PointLight')
    .filter(e => !(e.properties as PointLightProps).pulse);

  const pointLightsPulsed = objectList
    .filter(e => e.type === 'PointLight');

  const renderLight = (lightmap: LightMap) => e => {
    const properties = e.properties as PointLightProps;

    /**
     * Transform color format from `#aarrggbb` to `#rrggbbaa`
     * - Really strange thing from TiledMapEditor :/
     */
    const color = '#' + properties.color.substr(3, 6) + properties.color.substr(1, 2);

    lightmap.pointLight(e.x + 8, e.y - 8, properties.scale * 32, color);
  };

  pointLights.forEach(renderLight(this.lightmap));
  pointLightsPulsed.forEach(renderLight(this.lightmapPulsed));
}