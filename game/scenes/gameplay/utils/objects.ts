import { ObjectFactory } from "game/objects";

export function spawnObjects(): void {
  if (!this.mapWorker) {
    throw new Error('mapWorker in current scene isn\'t instance of TiledMap or doesn\'t exsist');
  }
  
  this.mapWorker.getObjectList().forEach(object => {

    if (!ObjectFactory.isHaveClass(object.type)) return;

    const instance = ObjectFactory.create(object.type, object.properties, object); // new Object(object.properties, object) as Actor;
    const isTileObject = !!object.gid;

    instance.position.x = object.x;
    instance.position.y = object.y;

    if (isTileObject) {
      instance.position.y -= object.height;
    }
    
    instance.refName = object.name || object.id;

    this.refs[instance.refName] = instance;
    this.addChild(instance);
  });

  this.objects.forEach(e => e.triggerEvent('spawn', { scene: this }));
}