export const drawing = {
  renderFPS(c: CanvasRenderingContext2D, fps: number): void {
    c.font = "16px sans-serif";
    const text = "FPS: " + (fps >> 0);
    const width = c.measureText(text).width + 16;

    c.fillStyle = "#fff";
    c.fillRect(16, 16, width, 24);
    c.fillStyle = "#000";
    c.fillText(text, 16 + 8, 16 + 16);
  },

  renderFade (c: CanvasRenderingContext2D): void {
    c.save();
    c.fillStyle = '#000';
    c.globalAlpha = this.fade;
    c.fillRect(0, 0, c.canvas.width, c.canvas.height);
    c.restore();
  }
};