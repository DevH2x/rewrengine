import { Actor, animate, CollisionMap, ITiledmapData, LightMap, Scene, Tiledmap } from "@core";
import mapList from 'game/maps';
import { BlackZone } from "game/objects/BlackZone";
import { saveGameState } from "game/helpers/saveGameState";
import { gameInterface } from "game/helpers/gameInterface";
import { rendererInstance } from "game/engine/renderer";
import { initLighting } from "./utils/lighting";
import { spawnObjects } from "./utils/objects";
import { store } from "game/ui/store";
import { drawing } from "./utils/drawing";

export class Gameplay extends Scene {
  public mapWorker: Tiledmap = new Tiledmap;
  
  private lightmap: LightMap;
  private lightmapPulsed: LightMap; 
  
  public solidObjects: Actor[] = [];
  public collisionMap: CollisionMap;


  public refs: {[key: string]: Actor } = {};
  public isStaticCamera = false;
  public fade = 1;
  public currentArea: BlackZone;

  constructor () {
    super();

    this.setViewport();
    this.initEventListeners();
  }

  private setViewport() {
    this.viewport = {
      ...this.viewport,
      width: rendererInstance.Canvas.width / 2,
      height: rendererInstance.Canvas.height / 2,
    };
  }

  private initEventListeners(): void {
    this.addListener('fade:in', (ms) => {
      animate(percent => {
        this.fade = percent;
      }, ms);
    });
    this.addListener('fade:out', (ms) => {
      animate(percent => {
        this.fade = 1 - percent;
      }, ms);
    });
    gameInterface.addListener('request:pause', () => {
      store.commit('pause/SET', !store.state.isPaused);
    });

    gameInterface.addListener('request:map', map => {
      this.loadMap(mapList[map]);
    });
  }

  public saveGame (): string {
    return saveGameState(this);
  }

  public load (): void {
    this.loadMap(mapList['c1m1']);
  }

  private reset () {
    this.currentArea = null;
    this.solidObjects = [];
    this.refs = {};
    this.destroyObjects();
  }

  private async loadMap (file: ITiledmapData) {
    this.triggerEvent('fade:in', 500);
    await animate(() => true, 1000);
    const { mapWorker } = this;
    this.reset();

    mapWorker.addListener('load', async () => {
      this.collisionMap = new CollisionMap(this.mapWorker.getCanvasByLayerName('$solid'));
      this.initLighting();
      this.spawnObjects();
      
      this.triggerEvent('fade:out', 1000);
    }, { once: true });
    
    mapWorker.load(file);
  }
  private initLighting = initLighting
  private spawnObjects = spawnObjects

  private renderMap(c: CanvasRenderingContext2D) {
    this.mapWorker.render(c);
  }

  private renderLight (c: CanvasRenderingContext2D) {
    const { lightmap, lightmapPulsed } = this;
    if (Math.random() < 0.05) {
      lightmapPulsed?.render(c);
    } else {
      lightmap?.render(c);
    }
  }

  private controlCamera() {
    let tx = 0, ty = 0; // targetPoint;

    if (this.refs.plr && this.currentArea) {
      const { position: p } = this.refs.plr;
      const { position: ap, bbox: { width: bw, height: bh } } = this.currentArea;
      const { width: vw, height: vh } = this.viewport;

      tx = Math.max(ap.x + vw / 2, Math.min(ap.x + bw - vw / 2, p.x));
      ty = Math.max(ap.y + vh / 2, Math.min(ap.y + bh - vh / 2, p.y));
    }

    this.viewport.x -= (this.viewport.x - tx) / 4; 
    this.viewport.y -= (this.viewport.y - ty) / 4; 
  }

  private renderFPS = drawing.renderFPS
  private renderFade = drawing.renderFade

  public staticForeground (c: CanvasRenderingContext2D, dt: number, fps: number): void {
    this.renderFade(c);
    this.renderFPS(c, fps);
    this.controlCamera();
  }

  public beforeUpdate(c: CanvasRenderingContext2D): void {
    this.renderMap(c);
  }

  public afterUpdate (c: CanvasRenderingContext2D): void {
    this.renderLight(c);
  }
}