import { Scene } from "@core";
import { rendererInstance } from "game/engine/renderer";
import { Water } from "game/objects/Water";

export class WaterText extends Scene {
  private particles = [];
  private water = new Water({}, {
    x: -rendererInstance.Canvas.width / 2, 
    y: rendererInstance.Canvas.height - 200, 
    width: rendererInstance.Canvas.width, 
    height: 200
  });
  constructor () {
    super();
    
    this.viewport = {
      ...this.viewport,
      width: rendererInstance.Canvas.width,
      height: rendererInstance.Canvas.height,
    };
    
    const { water } = this;

    this.addChild(water);
  }

  beforeUpdate(c: CanvasRenderingContext2D): void {
    c.fillStyle = "#000";
    c.fillRect(-c.canvas.width / 2, -c.canvas.height / 2, c.canvas.width, c.canvas.height);


    this.particles.forEach((e, i) => {
      if (!e) return;

      e.speed += 0.5;
      e.y += e.speed;

      c.fillStyle = "#900";
      c.fillRect(e.x - 1, e.y, 2, e.speed * 2);
    });
  }
}