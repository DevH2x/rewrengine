import { MouseInput, Scene } from "@core";
import { rendererInstance } from "game/engine/renderer";

export class Network extends Scene {
  public mouse = new MouseInput(rendererInstance.Canvas);
  public staticForeground(c): void {
    const { mouse } = this;
    c.fillStyle = '#fff';
    c.fillRect(mouse.x - 1, mouse.y - 1, 2, 2);
  }
}