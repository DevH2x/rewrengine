// HARDCODE PROTOTYPE
/* eslint-disable */
const baseTiledData = JSON.parse(`{
  "backgroundcolor": "#191919",
  "compressionlevel": -1,
  "nextlayerid":34,
  "nextobjectid":42,
  "orientation":"orthogonal",
  "renderorder":"right-down",
  "editorsettings": {
    "export": {
      "target": "."
    }
  },
  "tiledversion":"1.4.3",
  "tileheight":16,
  "tilesets":[
    {
    "columns":4,
    "firstgid":1,
    "image":"..\/..\/dist\/gfx\/solid.png",
    "imageheight":32,
    "imagewidth":64,
    "margin":0,
    "name":"solid",
    "spacing":0,
    "tilecount":8,
    "tileheight":16,
    "tilewidth":16
    }
  ],
  "tilewidth":16,
  "type":"map",
  "version":1.4,
  "width": 200,
  "height": 100,
  "layers": [
    {
      "data": [],
      "height":100,
      "id":31,
      "name":"details",
      "opacity":1,
      "type":"tilelayer",
      "visible":true,
      "width":200,
      "x":0,
      "y":0
    }
  ]
}
`);
const c = document.createElement('canvas').getContext('2d');
const { canvas } = c;

canvas.width = 200;
canvas.height = 100;
canvas.style.imageRendering = 'pixelated';

c.fillStyle = "#000";
c.fillRect(0, 0, canvas.width, canvas.height);
const _depth = 5;
const generate = (x, y, width, height, depth = _depth) => {
  if (depth < 0 || isNaN(depth)) return;
  if (width < 12 || height < 12) return;
  depth -= 1;
  
  const vertical = Math.random() > 0.7;
  const percentSlice = Math.max(0.3, Math.min(0.7, Math.random()));
  let nWidth = width;
  let nHeight = height;
  
  if (vertical) {
  	nWidth *= percentSlice;
  } else {
  	nHeight *= percentSlice;
  }
  
  return {
  	x,
    y,
  	width,
    height,
    children: [
    	generate(x, y, nWidth, nHeight, depth),
      generate(
      	vertical ? x + nWidth : x,
        vertical ? y : y + nHeight,
      	vertical ? (width - nWidth) : width,
        vertical ? height : (height - nHeight), depth
      )
    ].filter(e => !!e),
    
    render () {
    	if (this.children.length) return this.children.forEach(e => e && e.render());
    	c.fillStyle = `#${(Math.random() * 15 >> 0).toString(16)}${(Math.random() * 15 >> 0).toString(16)}${(Math.random() * 15 >> 0).toString(16)}`;
      c.strokeStyle = '#fff';
      // c.strokeRect(this.x + 1, this.y + 1, this.width - 2, this.height - 2);
      
      const width = Math.max(0.5, Math.random()) * (this.width);
      const height = Math.max(0.5, Math.random()) * (this.height);
      
      c.fillRect(this.x + this.width / 2 - width / 2, this.y + this.height / 2 - height / 2, width, height);
      
      this.children.forEach(e => e && e.render());
    }
  };
};
const space = generate(0, 0, canvas.width, canvas.height);
space.render();
const arrayData = [...c.getImageData(0, 0, canvas.width, canvas.height).data.filter((e, i) => i % 4 === 0).map(e => Math.min(1, e))];
baseTiledData.layers[0].data = arrayData;
console.log(JSON.stringify(baseTiledData));


document.body.appendChild(canvas);
