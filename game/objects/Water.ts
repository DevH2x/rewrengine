import { Actor } from "@core";
import { distortionMapContext } from "game/shaders/water";
import { Gameplay } from "game/scenes/GamePlay";
import { PhysicsActor } from "./PhysicsActor";

export class Water extends Actor {
  static ANGLE_SIZE = 5;
  static K = 0.002
  static V = 0.005
  
  private angles;
  private frames = 0;

  public scene: Gameplay;
  public depth = 0;
  
  constructor (properties, data) {
    super();

    this.bbox.change({
      x: 0,
      y: 0,
      width: data.width,
      height: data.height
    });

    const { width } = this.bbox;

    this.angles = new Array((width / Water.ANGLE_SIZE>>0) + 2).fill(0).map(e => {
      return {
        pos: 0,
        vel: 0,
        ofs: 0
      };
    });
  }

  get PhysicsObjectList (): Actor[] {
    return this.scene.objects.filter((e: PhysicsActor) => e.isPhysics);
  }

  public update (c: CanvasRenderingContext2D, dt: number): void {

    if (!this.isInViewport) return;
    const { x, y } = this.position;
    const { width, height } = this.bbox;
    
    c.save();
    c.fillStyle = "#333";

    c.lineWidth = 2;
     
    c.beginPath();
    c.moveTo(x + width, y + height);
    c.lineTo(x, y + height);
     
    for (let i = 0; i < 2; i++) {
      this.angles.forEach((e, i) => {
        const X = (e.pos - e.ofs);
        const ACC = -Water.K * X - e.vel * 0.02;

        e.vel += ACC;
        e.pos += e.vel;


        const leftWave = this.angles[i - 1];
        const rightWave = this.angles[i + 1];

        if (leftWave) {
          const dl = (e.pos - (leftWave.pos || 0)) * 0.002;
          leftWave.vel += dl * 10;
          leftWave.pos += dl;
        }

        if (rightWave) {
          const dr = (e.pos - (rightWave.pos || 0)) * 0.002;
          rightWave.vel += dr * 10;
          rightWave.pos += dr;
        }

      });
    }
     
    this.angles.forEach((e, i) => {
      // e.vel += Math.random() * 0.02;
      c.lineTo(Math.min(x + width, x + i * Water.ANGLE_SIZE), y + e.pos);
    });

    c.globalCompositeOperation = "multiply";
    c.fill();
    c.restore();

    this.renderToDistortMap(dt);
  }

  afterUpdate (): void {
    this.PhysicsObjectList.forEach((actor: PhysicsActor) => {
      if (this.bbox.isPointInside(actor.position)) {
        const distY = actor.position.y - this.position.y;
        const distPercent = Math.max(0, Math.min(1, distY / (this.bbox.height / 2)));
        actor.isInWater = true;
        this.splash(actor.position.x, actor.speed.magnitude * (1 - distPercent));
      }
    });
  }
  
  private splash (x = 0, power = 0) {
    x = (x - this.position.x) / Water.ANGLE_SIZE >> 0;
    if (!this.angles[x]) return;
    this.angles[x].pos += -power * 0.016;
  }

  private renderToDistortMap(dt) {
    const c = distortionMapContext;
    this.frames += dt * 10;
    const { x, y } = this.position;
    const { width, height } = this.bbox;
    
    c.save();
    this.scene.setViewportSettingsToContext(c);
    c.strokeStyle = '#000';
    c.fillStyle = c.createLinearGradient(x, y, x, y + height);
    c.fillStyle.addColorStop(0, "#f00");
    c.fillStyle.addColorStop(0.8, "#f00");
    c.fillStyle.addColorStop(1, "#000");

    c.beginPath();
    c.moveTo(x + width, y + height);
    c.lineTo(x, y + height);
    this.angles.forEach((e, i) => {
      c.lineTo(Math.min(x + width, x + i * Water.ANGLE_SIZE), y + e.pos);
    });

    c.fill();
    c.fillStyle = c.createLinearGradient(x, y, x + width, y);
    c.fillStyle.addColorStop(0, "#000");
    c.fillStyle.addColorStop(0.2, "rgba(0,0,0,0)");
    c.fillStyle.addColorStop(0.8, "rgba(0,0,0,0)");
    c.fillStyle.addColorStop(1, "#000");
    c.fill();
    c.restore();
  }
}