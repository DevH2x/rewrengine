import { Actor, animate, BorderBox, ease, IMapObject } from '@core';
import { gameInterface } from 'game/helpers/gameInterface';
import { Gameplay } from 'game/scenes/GamePlay';
import { Activator } from './Activator';

interface IVerticalDoor {
  /**
   * Door opening percentage (from 0 to 1)
   */
  openValue: number;

  /**
   * Activator actor instance name
   */
  activator: string | number;
}

const DEFAULT_OPTIONS: IVerticalDoor = {
  openValue: 1,
  activator: null,
};

export class VerticalDoor extends Actor {
  private openedSize: number;
  private activatorInstance: Activator;
  public scene: Gameplay;
  public properties: IVerticalDoor;

  constructor(properties: IVerticalDoor, private object: IMapObject) {
    super(properties);

    this.properties = {
      ...DEFAULT_OPTIONS,
      ...this.properties,
    };

    this.bbox.change({
      height: object.height,
      width: object.width,
    });

    this.addListener(
      'spawn',
      () => {
        this.openedSize =
          Math.max(0, Math.min(1, this.properties.openValue)) *
          this.object.height;
        this.activatorInstance = this.scene.refs[
          this.properties.activator
        ] as Activator;

        this.initEventListeners();
        this.scene.solidObjects.push(this);
      },
      { once: true },
    );
  }

  private initEventListeners(): void {
    if (this.activatorInstance)
      this.activatorInstance.addListener('activestatechanged', active => {
        animate(percent => {
          percent = ease.easeInQuad(percent);
          if (!active) {
            percent = 1 - percent;
          }
          this.bbox.change({
            height: this.object.height - this.openedSize * percent,
          });
        }, 500);
      });
  }

  public beforeUpdate(c: CanvasRenderingContext2D, dt: number): void {
    c.fillStyle = '#000';
    c.fillRect(...this.position.array, this.bbox.width, this.bbox.height >> 0);
  }
}
