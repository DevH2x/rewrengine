/**
 * @file Player.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 

import { BorderBox, Sprite, Vector2, Keyboard } from "@core";
import { controls } from "game/controls";
import { gameInterface } from "game/helpers/gameInterface";
import { PhysicsActor } from "./PhysicsActor";


const defaultSpriteOptions = () => ({
  frameSize: new Vector2(16, 16),
  animationSpeed: 0.3,
  offset: new Vector2(0.5, 0.5)
});

enum EAccelerationState {
  WATER = 40,
  GROUND = 40,
  AIR = 2,
  ROPE = 7
}

enum EFrictionState {
  WATER = 0.9,
  AIR = 0.98,
  GROUND = 0.8,
  IDEAL = 1
}

enum EPlayerState {
  SWIM,
  RUN,
  IDLE,
  FLY,
  FALL
}

export class Player extends PhysicsActor {
  public spriteList = {
    'run': new Sprite({
      url: require('../graphics/player-run.png').default,
      ...defaultSpriteOptions(),
    }),

    'idle': new Sprite({
      url: require('../graphics/player-idle.png').default,
      ...defaultSpriteOptions()
    }),

    'jump': new Sprite({
      url: require('../graphics/player-jump.png').default,
      ...defaultSpriteOptions()
    }),

    'fall': new Sprite({
      url: require('../graphics/player-fall.png').default,
      ...defaultSpriteOptions()
    })
  };

  private defaultAnimationSpeed = 0.3;
  private accelerationValue = EAccelerationState.AIR;
  private playerState: EPlayerState;
  private isJumpPressed = false;
  private isUsePressed = false;
  private isControlsActive = true;
  protected jumpSpeed = 250;


  public sprite = 'idle';
  public bbox:BorderBox = this.bbox.change({ x: 4, y: 4, width: 8, height: 12 });

  constructor () {
    super();
    this.initEventListeners();
  }

  private initEventListeners() {
    this.addListener('press:jump', () => this.jump());
    this.addListener('press:use', () => gameInterface.triggerEvent('use', this));

    const deactivateEvent = this.addListener('deactivate', () => {
      this.isControlsActive = false;
    });

    const activateEvent = this.addListener('activate', () => {
      this.isControlsActive = true;
    });

    this.addListener('spawn', () => {
      this.position.add(new Vector2(8, 8));
    }, { once: true });

    this.addListener('destroy', () => {
      this.removeListener('deactivate', deactivateEvent);
      this.removeListener('activate', activateEvent);
    }, { once: true });
  }

  private updateAccelerationState () {
    this.accelerationValue = 
      this.isInWater ? EAccelerationState.WATER :
        this.isOnGround ? EAccelerationState.GROUND :
          this.isAttached ? EAccelerationState.ROPE :
            EAccelerationState.AIR;
  }

  private updateFrictionState () {
    this.friction.x =
      this.isInWater ? EFrictionState.WATER :
        this.isOnGround ? EFrictionState.GROUND :
          EFrictionState.AIR;

    this.friction.y =
      this.isInWater ? EFrictionState.WATER :
        EFrictionState.IDEAL;
  }

  private updatePlayerState () {
    this.playerState = 
      this.isInWater ? EPlayerState.SWIM :
        this.isOnGround ?
          this.isMoving ? EPlayerState.RUN : EPlayerState.IDLE :
          this.isFalling ? EPlayerState.FALL : EPlayerState.FLY; 
  }

  private updateAnimationState (dt: number) {
    switch (this.playerState) {

    case EPlayerState.SWIM:
      this.animationSwim(dt);
      break;

    case EPlayerState.IDLE:
      this.animationIdle();
      break;

    case EPlayerState.RUN:
      this.animationRun();
      break;

    case EPlayerState.FALL:
    case EPlayerState.FLY:
      this.animationFlyFall(dt);
      break;
    }
    this.scale.x = Math.sign(this.acceleration.x) || this.scale.x;
  }

  private animationSwim(dt: number) {
    const swimDirection = Math.atan2(this.speed.x, this.speed.y - 0.1) - Math.PI;
    this.angle -= (this.angle - (-this.scale.x * swimDirection)) / 4;
    this.speed.y += Math.sin(this.position.x / 5) * 0.6 * Math.sign(Math.abs(this.speed.x >> 0)) * dt;
    this.sprite = 'fall';
  }

  private animationIdle () {
    this.currentSprite.animationSpeed = this.defaultAnimationSpeed;
    this.sprite = 'idle';
    this.angle = 0;
  }
  
  private animationRun () {
    this.currentSprite.animationSpeed = Math.abs(this.speed.x / 300);
    this.sprite = 'run';
    this.angle = 0;
  }

  private animationFlyFall (dt) {
    this.angle = (Math.sign(this.speed.x) * -this.speed.y * this.speed.x) / 80000;
    this.sprite = this.speed.y < 0 && 'jump' || 'fall';
  }

  private controls() {
    if (!this.isControlsActive) {
      return;
    }

    this.acceleration.x = (Keyboard.isKeyPressed(controls.RUN_RIGHT) - Keyboard.isKeyPressed(controls.RUN_LEFT)) * this.accelerationValue;
    this.acceleration.y = this.isInWater ? (Keyboard.isKeyPressed(controls.SWIM_DOWN) - Keyboard.isKeyPressed(controls.SWIM_UP)) * this.accelerationValue : 0;

    if (Keyboard.isKeyPressed(controls.JUMP)) {
      if (!this.isJumpPressed) {
        this.triggerEvent('press:jump');
      }
      this.isJumpPressed = true;
    } else {
      this.isJumpPressed = false;
    }

    if (Keyboard.isKeyPressed(controls.USE)) {
      if (!this.isUsePressed) {
        this.triggerEvent('press:use');
      }
      this.isUsePressed = true;
    } else {
      this.isUsePressed = false;
    }
  }

  public beforeUpdate(c: CanvasRenderingContext2D, dt?: number): void {
    this.friction.x = this.isOnGround || this.isInWater ? 0.8 : 0.99;
    this.updateAccelerationState();
    this.controls();
    this.updateFrictionState();
    this.updatePlayerState();
    this.updateAnimationState(dt);
    super.beforeUpdate?.(c, dt);
  }
}
