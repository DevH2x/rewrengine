import { Actor } from "@core";
import { Gameplay } from "game/scenes/GamePlay";

export interface Item {
    visualize?(c: CanvasRenderingContext2D): void
}

export class Item extends Actor {
    public scene: Gameplay;
    public checkRadius = 32;
    public name = 'noName';

    private checkPlayerInside () {
      const { plr } = this.scene.refs;

      if(!plr) return;

      if (plr.position.distance(this.position) < 32) {
        plr.triggerEvent('pickedupitem', this);
        this.destroy();
      }
    }

    public beforeUpdate(c: CanvasRenderingContext2D): void {
      this.checkPlayerInside();
      this.visualize?.(c);
    }
}