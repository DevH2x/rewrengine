import { Actor, Sprite, Vector2 } from "@core";
const VENT_SPRITE = require('../graphics/vent.png').default;

export class Vent extends Actor {
  public spriteList = {
    'default': new Sprite({
      url: VENT_SPRITE,
      offset: new Vector2(0.5, 0.5)
    })
  }

  public sprite = 'default'

  public rotateSpeed = 0.1 + Math.random() * 0.5;

  constructor () {
    super();

    this.bbox
      .change({
        width: 32,
        height: 32,
        x: 16,
        y: 16
      });
  }
  
  update (): void { return; }
  
  afterUpdate (c: CanvasRenderingContext2D, dt: number): void {
    this.angle += this.rotateSpeed;
    super.update(c, dt);
  }
}