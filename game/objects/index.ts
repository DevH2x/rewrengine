import { Actor } from "@core";

const loadObjectClass = require.context('./', false, /\.ts$/);

export abstract class ObjectFactory {
  static objectList: { [key: string]: ObjectConstructor } = {}
  
  static registerClass (typeName: string, constructor: ObjectConstructor): void {
    ObjectFactory.objectList[typeName] = constructor;
  }

  static create (type: string, ...args: any[]): Actor {
    return new ObjectFactory.objectList[type](...args) as Actor;
  }

  static isHaveClass(type: string): boolean {
    return !!ObjectFactory.objectList[type];
  }
}

loadObjectClass.keys().forEach((path: string) => {
  const className = path.split('/').pop().split('.')[0];
  const module = loadObjectClass(path);
  ObjectFactory.registerClass(className, module[Object.keys(module)[0]]);
});