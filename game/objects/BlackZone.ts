import { Actor, IMapObject } from "@core";
import { Gameplay } from "game/scenes/GamePlay";

export class BlackZone extends Actor {
  public depth = 20;
  public scene: Gameplay;

  private _active = false;

  constructor (properties = {}, object: IMapObject) {
    super(properties);
    const { width, height } = object;
    this.bbox.change({ width, height });
  }

  set active (value) {
    if (value !== this._active) {
      this._active = value;

      if (value) {
        this.scene.currentArea = this;
      }
    }
  }

  get isPlayerInside (): boolean {
    return this.bbox.isPointInside(this.scene.refs.plr.position);
  }

  afterUpdate (): void {
    this.active = this.isPlayerInside;
  }
}