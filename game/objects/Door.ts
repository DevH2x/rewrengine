import { Actor, animate, delay, Sprite, Vector2 } from "@core";
import { gameInterface } from "game/helpers/gameInterface";
import type { IRenderPipeline } from "@core";
import type { Gameplay } from "game/scenes/GamePlay";
import { PhysicsActor } from "./PhysicsActor";

interface IDoor {
  moveToRel: string,
}

export class Door extends Actor implements IRenderPipeline {

  public spriteList = {
    'default': new Sprite({
      url: require('../graphics/door-unlocked.png').default,
      frameSize: new Vector2(32, 48),
      animationSpeed: 0
    }),

    'locked': new Sprite({
      url: require('../graphics/door-locked.png').default
    })
  }

  public isLocked = false;

  protected scene: Gameplay;

  constructor (public properties: IDoor) {
    super(properties);

    this.bbox
      .change({
        width: 32,
        height: 48
      });

    gameInterface.addListener('use', (actor: Actor & PhysicsActor) => {
      if (this.bbox.isPointInside(actor.position)) {
        this.transferActor(actor);
      }
    });
  }

  private get targetDoor (): Actor {
    return this.scene.refs[this.properties.moveToRel] || this;
  }

  private teleportActor(actor: Actor | PhysicsActor) {
    const { targetDoor } = this; 
    const relPos = this.bbox.getRelativePosition(actor.position);

    actor.position.set(targetDoor.position.additive(relPos));
    this.scene.viewport.x = actor.position.x;
    this.scene.viewport.y = actor.position.y;
  }

  private async animateOpenDoor() {
    await animate((percent) => {
      this.currentSprite.currentFrame = ((this.currentSprite.framesCount.x - 1) * percent) >> 0;
    }, 500);
  }

  private async animateCloseDoor () {
    this.scene.triggerEvent('fade:out', 1000);

    await animate((percent) => {
      percent = 1 - percent;
      this.currentSprite.currentFrame = ((this.currentSprite.framesCount.x - 1) * percent) >> 0;
      this.targetDoor.currentSprite.currentFrame = this.currentSprite.currentFrame;
    }, 500);
  }

  private async transferActor(actor: Actor & PhysicsActor) {
    const { isLocked } = this;

    if (!isLocked) {
      actor.triggerEvent('deactivate', this);

      await this.animateOpenDoor();

      if (!this.bbox.isPointInside(actor.position)) {
        const targetPosition = this.bbox
          .getRelativePosition(actor.position)
          .additive(new Vector2(-this.bbox.width / 2, 0));
        
        const startPosition = actor.position.clone() ;

        await animate(percent => {
          actor.position.x = startPosition.x - targetPosition.x * percent;
          actor.speed.x = Math.sign(this.bbox.center.x - actor.position.x);
        }, 500);
      }

    
      this.scene.triggerEvent('fade:in', 400);
      await delay(400);

      this.teleportActor(actor);
      await this.animateCloseDoor();

      actor.triggerEvent('activate', this);
    }
  }
}