import { Actor } from "@core";

export abstract class Activator extends Actor {
    private _isActive = false;

    protected set isActive (value: boolean) {
      if (value !== this._isActive) {
        this.triggerEvent('activestatechanged', value);
        this._isActive = value;
      }
    }

    protected get isActive (): boolean {
      return this._isActive;
    }
}