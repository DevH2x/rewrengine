/**
 * PROTOTYPE
 */
import { Actor, Color, Vector2 } from '@core';
import { timeStamp } from 'console';
import { gameInterface } from 'game/helpers/gameInterface';
import { Gameplay } from 'game/scenes/GamePlay';
import { PhysicsEntity } from './Physics/Entity';
import { Vertex } from './Physics/Vertex';
import { PhysicsActor } from './PhysicsActor';

export interface IJoint {
  position: Vector2;
  velocity: Vector2;
  mass: number;
  pinned: boolean;
}

interface IPolylinePoint {
    x: number,
    y: number
}
interface IRope {
  color: string,
  unpinLast: boolean,
  unpinFirst: boolean,
  width: number
}

interface IConnectData {
  actor: PhysicsActor,
  vertex: Vertex,
  refName: string | number
}

enum EUnpinState {
  FIRST = 1,
  LAST = 2,
  POHUI = 0
}
export class Rope extends PhysicsEntity {
  private jointDistance = 20;
  public scene: Gameplay;
  static DEFAULT_PROPERTIES: IRope = {
    color: '#f00',
    unpinLast: false,
    unpinFirst: false,
    width: 2
  }

  private physicsActors: PhysicsActor[] = [];
  private connectedTo: IConnectData[] = [];

  private allowConnect = true;
  private coldown = 1000;

  constructor(public properties: IRope, private object) {
    super();

    this.applyProperties();
    this.addListener('spawn', () => {
      this.constructRope(this.object.polyline);
      this.physicsActors = this.scene.objects.filter((e: Actor & PhysicsActor) => !!e.speed) as PhysicsActor[]; 
    });
  }

  private applyProperties (): void {
    this.properties = {
      ...Rope.DEFAULT_PROPERTIES,
      ...(this.properties || {}),
    };
    const { properties } = this;
    const color = '#' + properties.color.substr(3, 6) + properties.color.substr(1, 2);

    this.boneWidth = this.properties.width;
    this.boneColor = color;
  }

  private constructRope (polyline: IPolylinePoint[]): void {
    polyline.forEach((p, i, a) => {
      const isLast = i === a.length - 1;
      if (isLast) return;
      
      const v1 = this.position.additive(new Vector2(p.x, p.y));
      const v2 = this.position.additive(new Vector2(a[i + 1].x, a[i + 1].y));

      const isUnpinFirst = i === 0 && this.properties.unpinFirst ? EUnpinState.FIRST : EUnpinState.POHUI;
      const isUnpinLast = i === a.length - 2 && this.properties.unpinLast ? EUnpinState.LAST : EUnpinState.POHUI;

      this.generateLine(v1, v2, isUnpinFirst || isUnpinLast);
    });
  }

  private generateLine (v1: Vector2, v2: Vector2, unpinState: EUnpinState) {
    const { jointDistance } = this;
    const { x, y } = v1;
    const dist = v1.distanceV(v2).magnitude;
    const angle = v1.angleBetween(v2);
    const boneCount = (dist / jointDistance) >> 0;
    const vl = [];
    for (let i = 0; i <= boneCount; i++) {
      const vp = new Vector2(x + Math.cos(angle) * jointDistance * i, y + Math.sin(angle) * jointDistance * i);
      const vertexIndex = this.vertex(i === boneCount ? v2 : vp);

      if (!i || i === boneCount) {
        if (unpinState !== EUnpinState.FIRST && unpinState !== EUnpinState.LAST) {
          this.pinVertex(vertexIndex);
        }
      }

      vl.push(vertexIndex);
    }

    for (let i = 0; i < vl.length - 1; i++) {
      this.bone(vl[i], vl[i + 1], 2, jointDistance);
    }
  }

  public update(c: CanvasRenderingContext2D, dt: number): void {
    this.render(c);
    this.vertexList.forEach(e => {
      if (e.isPinned) return;

      if (!e.isActorConnected) {
        this.physicsActors.forEach(a => {
          const mag = e.position.distance(a.position);
          if (mag < 8) {
            e.position = e.position.additive(a.speed.multipleBy(dt));
          }
        });
      }

      if (e.isActive) {
        while(this.scene.collisionMap.checkCollision(e.position.x, e.position.y)) {
          e.position.y -= Math.sign(e.velocity.y) / 10;
          e.op = e.position.clone();
        }
      }
    });

    this.connectedTo.forEach(e => {
      e.actor.position.set(e.vertex.position.x, e.vertex.position.y + 6);
      e.vertex.position.add(e.actor.acceleration.multipleBy(dt));
    });

    if (this.allowConnect) {
      if (this.scene.refs.plr) {
        const actor = this.scene.refs.plr as PhysicsActor;
        if (!actor.isAttached) {
          const nearestVertex = this.getNearestVertexToActor(actor);

          if (nearestVertex.position.distance(actor.position) < 16) {
            this.connectActorToVertex(actor.refName, nearestVertex.id, dt);
            actor.addListener('jump', () => {
              this.disconnectActor(actor.refName);
              this.allowConnect = false;
              setTimeout(() => {
                this.allowConnect = true;
              }, this.coldown);
            }, { once: true });
          }
        }
      }
    }
  }

  public connectToActor(instance: string | number, vertexIndex: number): void {
    this.vertexList[vertexIndex].connectedPosition = this.scene.refs[instance].position;
  }

  public getNearestVertexToActor(actor: PhysicsActor): Vertex {
    return this.vertexList.reduce((p, c) => p.position.distance(actor.position) < c.position.distance(actor.position)? p : c, this.vertexList[0]);
  }

  public connectActorToVertex(refName: string | number, vertexIndex: number, dt: number): void {
    const actor = this.scene.refs[refName] as PhysicsActor;
    const vertex = this.vertexList[vertexIndex];
    this.connectedTo.push({
      actor,
      vertex,
      refName
    });

    vertex.isActorConnected = actor;
    actor.attachedToVertex = vertex;
    actor.isAttached = true;
    vertex.mass = 3;
    vertex.position.add(actor.speed.multipleBy(dt));
  }

  public disconnectActor(refName: string | number): void {
    this.connectedTo = this.connectedTo.filter(e => {
      if (e.refName !== refName) {
        return true;
      } else {
        e.vertex.isActorConnected = false;
        e.actor.isAttached = false;
        e.actor.attachedToVertex = null;
        e.actor.speed.add(e.vertex.velocity.multipleBy(0.5));
        e.vertex.mass = 1;
        return false;
      }
    });
  }
}
