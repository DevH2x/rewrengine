import { Vector2, Actor } from "@core";
import type { Gameplay } from "game/scenes/GamePlay";
import { Vertex } from "./Physics/Vertex";

export class PhysicsActor extends Actor {

  public speed = new Vector2(0, 0);
  public acceleration = new Vector2(0, 0);
  public friction = new Vector2(1, 1);
  public isPhysics = true;
  public isInWater = false;
  public isAttached = false;
  public isMoving = false
  public isFalling = false;
  public isActive = true;

  public attachedToVertex: Vertex;
  public scene: Gameplay;
  
  protected jumpSpeed = 250;
  
  static GRAVITY = 600;
  static WATER_GRAVITY = 0.01;
  
  constructor () {
    super();
  }

  protected get isOnGround (): boolean {
    const { collisionMap = false } = this.scene;
    if (!collisionMap) return false;
    const c = collisionMap.checkCollision.bind(collisionMap);

    return (
      c(this.bbox.left + 2, this.bbox.bottom + 2) || 
      c(this.position.x, this.bbox.bottom + 2) || 
      c(this.bbox.right - 2, this.bbox.bottom + 2)
    );
  }

  protected jump (): void {
    if (this.isOnGround || this.isInWater || this.isAttached) {
      this.speed.y = -this.jumpSpeed;
      this.speed.x *= 1.5;
      this.triggerEvent('jump', this);
    }
  }

  private movement (dt: number) {
    this.speed.x += this.acceleration.x;
    this.speed.y += this.acceleration.y;

    if (!this.isOnGround && !this.isAttached) {
      this.speed.y += (this.isInWater ? PhysicsActor.WATER_GRAVITY : PhysicsActor.GRAVITY) * dt;
    }
    this.speed.x *= this.friction.x;
    this.speed.y *= this.friction.y;

    this.isMoving = Math.abs(this.speed.x) > 0.2;
    this.isFalling = Math.abs(this.speed.y) > 0;
  }

  private checkCollisionWithSolidObjects(x: number, y: number): boolean {
    return !!this.scene.solidObjects.find(object => object.bbox.isPointInside(new Vector2(x, y)));
  }

  private checkSideCollisiton(x: number, y: number): boolean {
    const { collisionMap } = this.scene;
    const check = (x, y) => {
      if (collisionMap.checkCollision(x, y)) {
        return true;
      }

      return this.checkCollisionWithSolidObjects(x, y);
    };

    if (x) {
      for(let i = 0; i < this.bbox.height; i++) {
        if (
          check(
            this.bbox[x > 0 ? 'right' : 'left'] + x,
            this.bbox.top + i
          )
        ) {
          return true;
        }
      }
    }

    if (y) { 
      for(let i = 0; i < this.bbox.width; i++) {
        if (
          check(
            this.bbox.left + i, 
            this.bbox[y > 0 ? 'bottom' : 'top'] + y
          )
        ) {
          return true;
        }
      }
    }

    return false;
  }

  private collision (dt: number) {
    if (this.checkSideCollisiton(0, this.speed.y * dt)) {
      while (!this.checkSideCollisiton(0, Math.sign(this.speed.y))) {
        this.position.y += Math.sign(this.speed.y);
      }
      this.speed.y = 0;
    }
    if (this.checkSideCollisiton(this.speed.x * dt, 0)) {
      while (!this.checkSideCollisiton(Math.sign(this.speed.x), 0)) {
        this.position.x += Math.sign(this.speed.x);
      }
      this.speed.x = 0;
    }
  }

  private applyPhysics(dt) {
    this.position.x += this.speed.x * dt;
    this.position.y += this.speed.y * dt;
  }

  public beforeUpdate (c: CanvasRenderingContext2D, dt: number): void {
    this.movement(dt);
    this.collision(dt);
    super.beforeUpdate?.(c);
  }

  public afterUpdate (c: CanvasRenderingContext2D, dt?: number): void {
    this.applyPhysics(dt);
    super.afterUpdate?.(c, dt);
    this.isInWater = false;
  }
}