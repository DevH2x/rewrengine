export interface PointLightProps {
  pulse?: boolean
  color?: string
  scale?: number
}