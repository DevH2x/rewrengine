import { Vertex } from "./Vertex";


export class Bone {
  constructor (public p1: Vertex, public p2: Vertex, private stiffness = 2.2, private length: number) {
    this.length = (this.length || this.p1.position.distance(this.p2.position));
  }


  public update (): void {
    if (!this.p2) return;
    const { x: dx, y: dy, magnitude: dist } = this.p1.position.distanceV(this.p2.position);
    const diff = (this.length - dist) / dist * this.stiffness;

    if (dist > this.length * 3) {
      this.p1.connectedPosition = null;
    }

    const ox = dx * diff * 0.5;
    const oy = dy * diff * 0.5;

    let m1 = this.p1.mass + this.p2.mass;
    const m2 = this.p1.mass / m1;
    m1 = this.p2.mass / m1;

    if (!this.p1.isPinned) {
      this.p1.position.x -= ox * m1;
      this.p1.position.y -= oy * m1;
    }

    if (!this.p2.isPinned) {
      this.p2.position.x += ox * m2;
      this.p2.position.y += oy * m2;
    }
  }
}