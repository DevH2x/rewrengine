import { Vector2 } from "@core";
import { PhysicsActor } from "../PhysicsActor";


export class Vertex {
    public mass = 1;
    public velocity = new Vector2(0, 0);
    private gravity = new Vector2(0, PhysicsActor.GRAVITY / 1000);
    public op: Vector2 //old position
    public isPinned = false;
    public connectedPosition;
    public isActorConnected;

    constructor (
        public position: Vector2,
        public id: number
    ) {
      this.op = position.clone();
    }

    public get isActive (): boolean {
      return this.velocity.magnitude > 0.2;
    }

    public update (): void {
      if (this.connectedPosition) {
        this.position.set(this.connectedPosition);
        this.op = this.position.clone();
        return;
      }
      if (this.isPinned) return;
      this.velocity = this.position.substract(this.op);
      this.op = this.position.clone();
      this.position.set(this.position
        .additive(this.velocity)
        .additive(this.gravity)
      );
    }

    public render (c: CanvasRenderingContext2D): void {
      c.save();
      c.fillStyle = this.isActive ? '#f00' : '#fff';
      c.beginPath();
      c.arc(...this.position.array, 2, 0, Math.PI * 2);
      c.fill();
      c.font = '7px ibm';
      c.fillText(this.id.toString(), this.position.x >> 0, this.position.y >> 0);
      c.restore();
    }
}