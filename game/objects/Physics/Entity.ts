import { Actor, Vector2 } from "@core";
import { Bone } from "./Bone";
import { Vertex } from "./Vertex";

export class PhysicsEntity extends Actor {
    public boneList: Bone[] = [];
    public vertexList: Vertex[] = []; 
    protected boneWidth = 4;
    protected boneColor = '#f00';

    public vertex(p: Vector2): number {
      const v = this.vertexList.find(v => {
        return v.position.x === p.x && v.position.y === p.y;
      });
      if (v) return this.vertexList.indexOf(v);
      return this.vertexList.push(new Vertex(p, this.vertexList.length)) - 1;
    }

    public pinVertex (index: number): void {
      this.vertexList[index].isPinned = true;
    }

    public bone(pi1: number, pi2: number, stiffness = 1, length: number): void {
      this.boneList.push(new Bone(this.vertexList[pi1], this.vertexList[pi2], stiffness, length));
    }

    private calculate() {
      this.boneList.forEach(bone => {
        bone.update();
      });

      this.vertexList.forEach(vertex => {
        vertex.update();
      });
    }

    public render(c: CanvasRenderingContext2D): void {
      c.save();
      c.lineWidth = this.boneWidth;
      c.lineCap = 'round';
      c.lineJoin = 'round';
      c.strokeStyle = this.boneColor;
      c.beginPath();
      this.boneList.forEach((bone, i) => {
        if (i === 0) {
          c.moveTo(bone.p1.position.x, bone.p1.position.y);
        } else {
          c.lineTo(bone.p1.position.x, bone.p1.position.y);
        }
        c.lineTo(bone.p2.position.x, bone.p2.position.y);
      });
      c.stroke();
      c.restore();
    }

    public beforeUpdate (c: CanvasRenderingContext2D, dt: number): void {
      this.calculate();
    }
}