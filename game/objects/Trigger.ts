import { IMapObject } from '@core';
import { Gameplay } from 'game/scenes/GamePlay';
import { Activator } from './Activator';

interface ITrigger {
  refTarget: string | number;
  disabled: boolean;
}

export class Trigger extends Activator {
  public scene: Gameplay;
  public properties: ITrigger;
  
  constructor(properties: ITrigger, object: IMapObject) {
    super(properties);
    this.bbox.change({
      width: object.width,
      height: object.height,
    });
  }
  public beforeUpdate(c: CanvasRenderingContext2D): void {
    // c.strokeStyle = '#f00';
    // c.strokeRect(...this.position.array, this.bbox.width, this.bbox.height);

    if (!this.properties.disabled) {
      this.isActive = this.bbox.isPointInside(this.scene.refs.plr.position);
    }
  }
}
