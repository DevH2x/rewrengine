import { Key } from "@core";

export interface IControls {
    RUN_LEFT: number,
    RUN_RIGHT: number,
    SWIM_UP: number,
    SWIM_DOWN: number,
    JUMP: number,
    USE: number
}
export const controls: IControls = {
  RUN_LEFT: Key.LEFT,
  RUN_RIGHT: Key.RIGHT,
  SWIM_DOWN: Key.DOWN,
  SWIM_UP: Key.UP,
  JUMP: Key.SPACE,
  USE: 'X'.charCodeAt(0)
};