/**
 * @file index.ts
 * @copyright 2020 Radik Khamatdinov
 * @author Radik Khamatdinov <h2xdeveloper@gmail.com>
 * 
 * @license
 * Copyright (c) 2020 Radik Khamatdinov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/ 
import './ui';
import './shaders';
import * as Global from './global';
import { GFont, Keyboard, ResourceManager } from '@core';
import { store } from './ui/store';
import { postProcessPipeline } from './engine/postProcessPipeline';

new GFont('ibm', require('./fonts/olivetti.woff').default);


const prepareFonts = (c: CanvasRenderingContext2D) => {
  c.fillStyle = "#fff";
  c.font = "14px ibm";
  c.fillText(' ', 32, 32);
};
Keyboard.initialize();

Global.sceneDispatcher.load(Global.Scenes.GamePlay);
Global.renderer.render((...args) => {

  const [c] = args;

  prepareFonts(c);

  if (ResourceManager.isLoaded) {
    if (!store.state.isPaused) {
      Global.sceneDispatcher.render(...args);
    }
  } else {
    c.save();
    c.fillStyle = "#000";
    c.fillRect(0, 0, c.canvas.width, c.canvas.height);

    c.fillStyle = "#fff";
    c.font = '12px ibm';
    c.textAlign = "center";
    c.fillText("Loading...", c.canvas.width / 2, c.canvas.height / 2);
    c.restore();
  }

  postProcessPipeline.render(args[3]);
});

document.body.appendChild(postProcessPipeline.outputCanvas);

window['SystemCall'] = Global;