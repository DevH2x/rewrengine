import { Scene } from "@core";
import { renderer } from "game/global";

export const saveGameState = (scene: Scene) => {
  const data = [];

  // Encoding Headers
  const headers = {
    version: '0.1a',
    date: new Date().toISOString(),
    map: 'map'
  };

  const versionCode = btoa(headers.version);
  data.push(Object.values(headers).join('%'));


  // Encoding Objects
  const objectPositions = [];
  scene.objects.forEach(e => {
    objectPositions.push(...e.position.array);
  });
  data.push(objectPositions.join('%'));

  // Encoding Screenshots
  const screenshoot = renderer.Canvas.toDataURL('image/jpg', 'low');
  data.push(screenshoot);
  
  const encodedData = atob(btoa(data.join('.') + '.' + versionCode).replace(/E/g, '$'));

  return btoa(encodedData);
};