import { Renderer } from "@core";

const rendererInstance = new Renderer({
  width: 854,
  height: 480,
  scale: 1,
  pixelatedRendering: true,
  preventAppend: true
});

export { rendererInstance };