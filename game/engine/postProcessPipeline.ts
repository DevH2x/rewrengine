import { PostProcessPipeline } from "@core";
import { rendererInstance } from "./renderer";

export const postProcessPipeline = new PostProcessPipeline(rendererInstance.Canvas);