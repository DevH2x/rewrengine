precision mediump float;

uniform sampler2D u_canvas;
uniform vec2 u_resolution;
uniform float u_amount;
uniform float u_pixelRatio;

void main() {
  float blurAmount = u_amount * (.000001 / (u_pixelRatio * u_pixelRatio));
  vec2 xy = gl_FragCoord.xy/u_resolution.xy;
	vec2 center=u_resolution.xy/2.;
	vec2 diff=xy-center;
	float dist=dot(diff,diff)*0.0004;
	vec2 amount=vec2((center.x-xy.x)*blurAmount*dist,0.);
	vec4 tx;
	
	tx=texture2D(u_canvas, xy+amount*-0.02)*vec4(0.666,0.0,0.0,0.2);
	tx+=texture2D(u_canvas, xy+amount*-0.01)*vec4(0.333,0.25,0.0,0.2);
	tx+=texture2D(u_canvas, xy)*vec4(0.0,0.5,0.0,0.2);
	tx+=texture2D(u_canvas, xy+amount*0.01)*vec4(0.0,0.25,0.333,0.2);
	tx+=texture2D(u_canvas, xy+amount*0.02)*vec4(0.0,0.0,0.667,0.2);

	gl_FragColor = tx;
}