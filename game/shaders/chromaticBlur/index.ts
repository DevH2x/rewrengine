import { postProcessPipeline } from "game/engine/postProcessPipeline";

interface IChromaticBlur {
  amount: number
}

export const chromaticBlur = postProcessPipeline.addShader<IChromaticBlur>(require('./shader.glsl').default, 2);
chromaticBlur.uniforms.amount = 1;