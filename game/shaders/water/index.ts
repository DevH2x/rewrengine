import { postProcessPipeline } from "game/engine/postProcessPipeline";
import { rendererInstance } from "game/engine/renderer";
export const distortionMap = document.createElement('canvas');
export const distortionMapContext = distortionMap.getContext('2d');

rendererInstance.addListener('render:start', () => {
  distortionMapContext.clearRect(0, 0, distortionMap.width, distortionMap.height);
});

postProcessPipeline.addShader(require('./water.glsl').default, 0, (s) => {
  s.setTexture('u_distortionMask', distortionMap as any);
});