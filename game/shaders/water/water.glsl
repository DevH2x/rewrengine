precision mediump float;

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_canvas;
uniform sampler2D u_distortionMask;

void main() {
  float blurSize = 0.002;
  vec2 uv = gl_FragCoord.xy / u_resolution;
  vec4 d = texture2D(u_distortionMask, uv);
  float t = u_time / 200.;

  uv.x += d.r * sin(t + uv.y * 20.) * 0.002;
  uv.y += d.r * sin(t + uv.x * 20.) * 0.002;

  vec4 col = texture2D(u_canvas, uv);
  if (d.r > 0.0) {
    col += texture2D(u_canvas, uv +vec2(blurSize,blurSize));
    col += texture2D(u_canvas, uv +vec2(-blurSize,blurSize));
    col += texture2D(u_canvas, uv +vec2(blurSize,-blurSize));
    col += texture2D(u_canvas, uv +vec2(-blurSize,-blurSize));
    col /= 5.0;
  }

  gl_FragColor = col;
}