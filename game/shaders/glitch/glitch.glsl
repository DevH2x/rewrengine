precision mediump float;

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_canvas;
uniform float u_amount;
uniform float u_speed;

//2D (returns 0 - 1)
float random2d(vec2 n) { 
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float randomRange (in vec2 seed, in float min, in float max) {
		return min + random2d(seed) * (max - min);
}

// return 1 if v inside 1d range
float insideRange(float v, float bottom, float top) {
   return step(bottom, v) - step(top, v);
}

void main() {
  float time = floor(u_time * u_speed * .001 * 60.);
  vec2 uv = gl_FragCoord.xy / u_resolution.xy;
  //copy orig
  vec3 outCol = texture2D(u_canvas, uv).rgb;
  
  //randomly offset slices horizontally
  float maxOffset = u_amount / 2.;
  for (float i = 0.; i < 10.; i += 1.) {
    float sliceY = random2d(vec2(time, 2345. + float(i))) * u_amount;
    float sliceH = random2d(vec2(time, 9035. + float(i))) * .25 * u_amount;
    float hOffset = randomRange(vec2(time, 9625. + float(i)), - maxOffset,maxOffset) * u_amount;
    vec2 uvOff = uv;
    
    uvOff.x += hOffset * u_amount;
    
    if(insideRange(uv.y, sliceY, fract(sliceY + sliceH)) == 1.)
    {
      outCol = texture2D(u_canvas, uvOff).rgb;
    }
  }
  
  //do slight offset on one entire channel
  float maxColOffset = u_amount / 6.;
  float rnd = random2d(vec2(time, 9545.));
  vec2 colOffset = vec2(randomRange(vec2(time, 9545.), -maxColOffset, maxColOffset),
  randomRange(vec2(time, 7205.), -maxColOffset, maxColOffset));
  
  if(rnd < .33){
    outCol.r = texture2D(u_canvas, uv + colOffset).r;
    
  } else if (rnd < .66) {
    outCol.g = texture2D(u_canvas, uv + colOffset).g;
  } else {
    outCol.b = texture2D(u_canvas, uv + colOffset).b;
  }
  
  gl_FragColor = vec4(outCol, 1.);
}