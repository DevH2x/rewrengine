import { postProcessPipeline } from "game/engine/postProcessPipeline";
import { rendererInstance } from "game/engine/renderer";


interface IGlitchSettings {
  amount: number,
  speed: number
}

export const glitch = postProcessPipeline.addShader<IGlitchSettings>(require('./glitch.glsl').default, 1);
rendererInstance.addListener('render:start', time => glitch.shader.setUniform('u_time', time));