import { ITiledmapData } from "@core";

const resolveMap = require.context('./', false, /.json$/);
const mapList: { [key: string]: ITiledmapData } = {};
resolveMap.keys().forEach(path => {
  const mapName = path.split('/').pop().split('.')[0];
  const module = resolveMap(path);

  mapList[mapName] = module;
});

export default mapList;