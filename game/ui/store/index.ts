import { Key, Scene } from '@core';
import { sceneDispatcher } from 'game/engine/sceneDispatcher';
import { gameInterface } from 'game/helpers/gameInterface';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export const store = new Vuex.Store({
  state: {
    isPaused: false,
    scene: 'noscene',
    isShowingMessage: false,
    messageData: {
      title: '',
      message: '',
      close: null
    }
  },

  mutations: {
    'scene/SET' (state, payload) {
      state.scene = payload;
    },

    'message/SET' (state, payload) {
      state.isShowingMessage = true;
      state.messageData = payload;
    },

    'message/HIDE' (state) {
      state.isShowingMessage = false;
      state.messageData.close?.();
    },
    'pause/SET' (state, payload) {
      state.isPaused = payload;
    }
  },

  actions: {
    showMessage ({ commit }, payload) {
      commit('message/SET', payload);
    }
  }
});

sceneDispatcher.addListener('scene:load', (scene: Scene) => {
  store.commit('scene/SET', scene.name);
});

window.addEventListener('keydown', (e) => {
  if (e.keyCode === Key.ESC)
    gameInterface.triggerEvent('request:pause');
});