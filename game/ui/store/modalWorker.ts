import { store } from ".";
interface IMessageData {
  title: string,
  message: string,
  close?: (...args) => any
}
export const showMessage = (payload: IMessageData): Promise<void> => {
  return new Promise<void>((resolve) => {
    store.dispatch('showMessage',
      {
        ...payload,
        close: () => {
          resolve();
        }
      });
  });
};