import { Debug } from "@core";
import { rendererInstance } from "game/engine/renderer";

export default {
  name: 'DefaultUI',
  mounted (): void {
    const container = this.$el;
    if (container) {
      container.style.width = rendererInstance.Canvas.width + 'px';
      container.style.height = rendererInstance.Canvas.height + 'px';
      container.classList.add('ui');
    } else {
      Debug.warn('The reference "container" is not found in component', this.$options._componentTag);
    }
  },
  methods: {
    showMessage (payload): Promise<void> {
      return new Promise<void>((resolve) => {
        this.$store.dispatch('showMessage',
          {
            ...payload,
            close: () => {
              resolve();
            }
          });
      });
    }
  }
};