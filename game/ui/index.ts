import Vue from "vue";
import { store } from "./store";
import App from './App.vue';

export const vue = new Vue({
  el: '#ui',
  render: h => h(App),
  store
});