import { VueConstructor } from "vue";

const loadComponent = require.context('./', false, /\.vue$/); 
export const components: { [key: string]: VueConstructor<Vue> } = {};

loadComponent.keys().forEach(path => {
  const name = path.split('/').pop().split('.')[0];
  components[name] = loadComponent(path).default;
});